from PyQt5.QtCore import QRunnable, Qt, QThreadPool,pyqtSignal,QObject,QThread
import testModule as tm
import tenmaLoad as load
import serial
import time
import subprocess
import serial.tools.list_ports
import os
import pylink
import esptool

import bmsCAN
import FCTMacro as FM
FCTMacro = FM.FCTMacro()

class testWorker(QObject):
    progress = pyqtSignal(str,str,str)
    finished = pyqtSignal()
    error = pyqtSignal(str,str)
    buttonSelect = pyqtSignal(str)
    dbUiUpdateCall = pyqtSignal(str)

    def __init__(self, ID, ID1, ID2, ID3):
        super().__init__()
        
        self.QRCode             = ID
        self.ID1                = ID1
        self.ID2                = ID2
        self.LogicQRCode        = ID3

        self.testModule         = tm.test_Module()

        self.canBus             = bmsCAN.canCommunication(FCTMacro.DBC_LOCATION, self.ID1[2:]+self.ID2[2:])

        self.ser                = 0
        self.testOrderCount     = 0
        self.failCount          = 0

        self.testStartTime      = 0

        self.notStarted         = "(10,100,255)"
        self.underway           = "(255,200,0)"
        self.Complete           = "(0,255,0)"
        self.Failed             = "(255,0,0)"
        self.skipped            = "(255,255,255)"

        self.bmsPingTick        = 0
        self.currentGain        = 0
        self.currentOffset      = 0
        self.statusPrevious     = ""
        self.TotalTestTime      = 0

    def initTestSetup(self):
        function = "initTestSetup"

        result = self.canBus.connectPort()
        if result != 1:
            return ("BMS CAN:","No CAN device found, Error Code: " +str(result))

        if FCTMacro.PDI_MODE != 1:
            self.serialPort = self.testModule.moduleConnect()
            if(self.serialPort == 1 or self.serialPort == 2):
                return "No test module found","Reconnect the test module and try again, Error Code: " +str(self.serialPort)
            else:
                self.ser = serial.Serial(self.serialPort, baudrate=9600, timeout=1)                                                                 #Initialize COM Port object
                self.ser.reset_input_buffer()
                self.ser.reset_output_buffer()

        if FCTMacro.PDI_MODE not in [1, 4, 5]:
            self.tenma  = load.TenmaLoad(FCTMacro.tenmaLoad_PID, FCTMacro.tenmaLoad_VID)
            result = self.tenma.loadConnect()
            if(result != 0):
                return "Constant current load connection failed", "Check Load power and connection, Error Code: " +str(result)
        
        if FCTMacro.PDI_MODE in [0, 2, 3, 5]:
            return self.connectJlink()
        
        return function, "PASS"   

    def outputError(self, status, flag=False):
        testtime = round(time.time()-self.testStartTime, 2)
        self.TotalTestTime = round(self.TotalTestTime + testtime,2)
        FCTMacro.errorLogger(test="TestNo: " + str(self.testOrderCount+1)+ ") " + status[0],msg=status[1],testTime=testtime)
        if status[0] == self.statusPrevious:
            self.progress.emit(str(self.testOrderCount),self.skipped, "Skipped")
        else:
            self.statusPrevious=status[0]
            # print(self.statusPrevious)
            FCTMacro.excelLogger(self.testOrderCount, status[1])
            if str(status[1]).find("PASS") == -1:
                self.failCount += 1
                self.progress.emit(str(self.testOrderCount),self.Failed, status[1] + " " + str(testtime) + "s")
                if flag:
                    self.testOrderCount = FCTMacro.TEST_COUNT-1
                    # self.error.emit(status[0], status[1])
                    # self.buttonSelect.emit("nextTest")
                    # self.finished.emit()
                    return 1
            else:
                self.progress.emit(str(self.testOrderCount),self.Complete, status[1] + " " + str(testtime) + "s")
        if FCTMacro.TEST_COUNT-1 > self.testOrderCount:
            self.testOrderCount=self.testOrderCount+1
            self.progress.emit(str(self.testOrderCount),self.underway," ")
        self.testStartTime=time.time()
        return 0
   
    def run(self):
        try:
            FCTMacro.errorLogFileOpen(self.ID1)
            FCTMacro.errorLogger(ecuid="QRCode: " + self.QRCode + ", ECUID: " + self.ID1[2:].upper()+self.ID2[2:].upper(), count=0)

            FCTMacro.excelLogFileOpen(self.ID1,self.QRCode,self.LogicQRCode,self.ID1[2:].upper()+self.ID2[2:].upper())

            self.progress.emit(" "," ",str(self.QRCode))
            self.testOrderCount = 0
            self.progress.emit(str(self.testOrderCount),self.underway, " ")
            self.testStartTime = time.time()
            result=("Default", "PASS")

            for i in range(len(FCTMacro.TEST_ARRAY)-1):
                result = eval("self."+FCTMacro.TEST_ARRAY[i]+"()")
                if FCTMacro.TEST_ARRAY[i] == "packFusePin":
                    res = self.packFusePinRelayOff()
                    FCTMacro.errorLogger(test=res[0],msg=res[1])
                elif FCTMacro.TEST_ARRAY[i] in ["chargeMosfetSC", "dsgMosfet", "currentCalibration", "AFEShutdownAndHOrLDWake"]:
                    res = self.defaultModeFCT()
                    FCTMacro.errorLogger(test=res[0],msg=res[1])
                if FCTMacro.TEST_ARRAY[i] in ["initTestSetup","initAFEs", "emulatorON", "pingBMS", "pingFCT", "flashTestcode", "AFEShutdownAndHWake","AFEShutdownAndHWakeMCU","AFEShutdownAndLWake","mcuFlash" ,"bootMode","ESPflashCode","heartBeat","Input_Ac_Supply"]:
                    if self.outputError(result, flag=True):
                        break
                        # FCTMacro.errorLogFileClose()
                        # FCTMacro.excelLogFileClose()
                        # return None
                else:
                    self.outputError(result)
            if FCTMacro.PDI_MODE == 0 or FCTMacro.PDI_MODE==3:
                res = self.defaultModeFCT()
                FCTMacro.errorLogger(test=res[0],msg=res[1])
            testString = "BMS"
            if FCTMacro.PDI_MODE==4:
                testString = "DB"
            if self.failCount > 0:
                self.dbUiUpdateCall.emit("BoardFail")
                self.outputError((testString + " Test Failed","Failed Test Count: " + str(self.failCount) + " Test Time: " + str(self.TotalTestTime) + "s"),flag=True)
            else:
                self.dbUiUpdateCall.emit("BoardPass")
                self.outputError((testString + " Test Passed","PASS" + " Test Time: " + str(self.TotalTestTime)),flag=True)

        except Exception as e:
            self.error.emit("Exception",str(e))
            FCTMacro.errorLogger(test="From run Exception: " + str(self.testOrderCount+1), msg=str(e))
        
        if FCTMacro.PDI_MODE == 0:
            res = self.emulatorOFF()
            FCTMacro.errorLogger(test=res[0],msg=res[1])
        self.buttonSelect.emit("nextTest") 
        self.finished.emit()
        FCTMacro.errorLogFileClose()
        FCTMacro.excelLogFileClose()
        return None

    def connectJlink(self):
        function = "connectJlink"
        retryCount=0
        while retryCount < 2:
            try:
                retryCount = retryCount + 1
                jlink = pylink.JLink()
                # time.sleep(0.2)
                jlink.open()
                # time.sleep(0.2)
                jlink.set_speed(speed = 4000)
                # time.sleep(0.2)
                jlink.set_speed(speed = 4000)
                # time.sleep(0.2)
                ret = jlink.set_tif(pylink.enums.JLinkInterfaces.SWD)
                # time.sleep(0.2)
                if ret==True:
                    jlink.close()
                    return function + " JLINK Found", "PASS"
                    # time.sleep(0.1)
                    # jlink.connect('S32K116', verbose=True)
                    # time.sleep(0.5)
                    # ret = jlink.target_connected()
                    # time.sleep(0.1)
                    # if ret==True:
                    #     if jlink.erase()==0:
                    #         time.sleep(0.2)
                    #         if (jlink.flash_file(file_loc,flash_start)==0):
                    #             time.sleep(0.5)
                    #             jlink.reset(ms=10, halt=False)  
                    #             time.sleep(0.5)
                    #             print("Code Flash successful")
                    #             return function, "PASS"
                    #         else:
                    #             result= (function, "Failed to Flash")
                    #     else:
                    #         result= (function, " Failed to Erase")
                    # else:
                    #     result =(function,"Connect fail to target")
                    #     jlink.close()
                else:
                    result =(function,"Jlink SWD Conversion Error")
                    jlink.close()
                # print(f"Jlink device connection attempt count: {retryCount}")
            except Exception as e:
                result = ("JLINK Exception", "Make Sure device is connected, Exception: " + str(e))
        return result

    def chargeMosfetSC(self):
        try:
            #for double safety by default emulator is off during this time
            function = "chargeMosfetSC" 
            result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.EMULATOR_OFF)
            if result == 0:
                return function, "Error Communication FCT Board EMULATOR_OFF"
            elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
                return function, "Request Denied EMULATOR_OFF"

            timeNow = time.time()
            while time.time()-timeNow < 11:
                result = self.tenma.getVoltage()
                if result < 1:
                    break
                if time.time()-timeNow > 10:
                    if result > 0.5:
                        return function, "Mosfet Partially ON while off " + str(result)

            result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.Load_CHG)
            if result == 0:
                return function, "Error Communication FCT Board Load_CHG"
            elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
                return function, "Request Denied Load_CHG"
            
            result = self.tenma.setLoadCurrent(value=int((FCTMacro.BMS_CELLCOUNT*(FCTMacro.EMULATOR_Voltage/1000))/FCTMacro.LOAD_CURRENT_DEFAULT), mode = "CR")
            if result != 0:
                if result == 1:
                    return function, "Failed to set Tenma value"
                elif result == 2:
                    return function, "Failed to set Tenma mode"
                elif result == 3:
                    return function, "Failed to set Tenma ON"
                    
            timeNow = time.time()
            while time.time()-timeNow < 4:
                result = self.tenma.read_FlowCurr()
                if result < FCTMacro.CURRENT_ACCURACY:
                    return function, "PASS"
            return function, "Failed " + str(result)
        except Exception as e:
            return function, "Exception: "+str(e)

    def emulatorON(self):
        function = "emulatorON"
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.EMULATOR_ON)
        if result == 0:
            return function, "Error Communication FCT Board EMULATOR_ON"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied EMULATOR_ON"

        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.H_WAKE)
        if result == 0:
            return function, "Error Communication FCT Board H_WAKE"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied H_WAKE"
        
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.L_WAKE)
        if result == 0:
            return function, "Error Communication FCT Board L_WAKE"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied L_WAKE"

        timeNow = time.time()
        while time.time()-timeNow < 11:
            result = self.tenma.getVoltage()
            if result < 1:
                break
            if time.time()-timeNow > 10:
                if result > 0.5:
                    return function, "Mosfet Partially ON while off " + str(result)

        return function, "PASS"

    def flashCode(self, functionName, file_loc ,flash_addr):
        retryCount=0
        while retryCount < 3:
            retryCount = retryCount + 1
            try:
                
                jlink = pylink.JLink(unsecure_hook = unsecure_device_hook)
                # time.sleep(0.2)
                jlink.open()
                # time.sleep(0.2)
                ##jlink.set_speed(speed = 400)
                # time.sleep(0.2)
                jlink.set_speed(speed = 400)
                # time.sleep(0.2)
                ret = jlink.set_tif(pylink.enums.JLinkInterfaces.SWD)
                # time.sleep(0.2)
                if ret==True:
                    time.sleep(0.1)                                     ##
                    if(FCTMacro.PDI_MODE==5):                           ##
                        jlink.connect('STM32G0B1CB', verbose=True)      ##
                    else:                                               ##
                        jlink.connect('S32K116', verbose=True)          ##
                    # time.sleep(0.5)
                    ret = jlink.target_connected()
                    # time.sleep(0.1)
                    if ret==True:
                        # if jlink.erase()==0:
                        #     time.sleep(0.2)
                        if(FCTMacro.PDI_MODE==5):
                            jlink.reset()                       ##
                            time.sleep(0.2)             ##
                            print("waited 0.2 sec")             ##
                            jlink.reset()     
                        if (jlink.flash_file(file_loc, flash_addr)==0):
                            # time.sleep(0.5)
                            jlink.reset(ms=10, halt=False)  
                            # time.sleep(0.5)
                            # print("Code Flash successful")
                            jlink.close()
                            return functionName + " " + str(file_loc), "PASS"
                            # else:
                            #     result= (function, "Failed to Flash")
                        else:
                            result= (functionName, " Failed to Erase")
                    else:
                        result =(functionName,"Connect fail to target")
                        jlink.close()
                else:
                    result =(functionName,"Jlink SWD Conversion Error")
                    jlink.close()
            except Exception as e:
                result = (functionName+": JLINK Exception", "Make Sure device is connected, Exception: " + str(e))
            # print(result)
            # print(f"Jlink device connection attempt count: {retryCount}")
        return result
    
    def flashBootloaderCode(self, functionName, file_loc ,flash_addr):
        retryCount=0
        while retryCount < 3:
            retryCount = retryCount + 1
            try:
                
                jlink = pylink.JLink(unsecure_hook = unsecure_device_hook)
                # time.sleep(0.2)
                jlink.open()
                # time.sleep(0.2)
                ##jlink.set_speed(speed = 400)
                # time.sleep(0.2)
                jlink.set_speed(speed = 400)
                # time.sleep(0.2)
                ret = jlink.set_tif(pylink.enums.JLinkInterfaces.SWD)
                # time.sleep(0.2)
                if ret==True:
                    time.sleep(0.1)                                     ##
                    if(FCTMacro.PDI_MODE==5):                           ##
                        jlink.connect('STM32G0B1CB', verbose=True)      ##
                    else:                                               ##
                        jlink.connect('S32K116', verbose=True)          ##
                    # time.sleep(0.5)
                    ret = jlink.target_connected()
                    # time.sleep(0.1)
                    if ret==True:
                        # if jlink.erase()==0:
                        #     time.sleep(0.2)
                        if(FCTMacro.PDI_MODE==5):
                            jlink.reset()                       ##
                            if jlink.erase()==0:                ##
                                    time.sleep(0.2)             ##
                                    print("Erased")             ##
                            jlink.reset()     
                        if (jlink.flash_file(file_loc, flash_addr)==0):
                            # time.sleep(0.5)
                            jlink.reset(ms=10, halt=False)  
                            # time.sleep(0.5)
                            # print("Code Flash successful")
                            jlink.close()
                            return functionName + " " + str(file_loc), "PASS"
                            # else:
                            #     result= (function, "Failed to Flash")
                        else:
                            result= (functionName, " Failed to Erase")
                    else:
                        result =(functionName,"Connect fail to target")
                        jlink.close()
                else:
                    result =(functionName,"Jlink SWD Conversion Error")
                    jlink.close()
            except Exception as e:
                result = (functionName+": JLINK Exception", "Make Sure device is connected, Exception: " + str(e))
            # print(result)
            # print(f"Jlink device connection attempt count: {retryCount}")
        return result

    def flashTestcode(self):
        function = "flashTestcode"
        return self.flashCode(function, FCTMacro.TestCode_location)
        
    def pingBMS(self):
        function = "pingBMS"
        self.canBus.switchBaudRate("500")            
        data = [FCTMacro.BMS_CELLCOUNT,0,0,0]
        result = self.canBus.sendCommandBMS(FCTMacro.PING_COMMAND, data=data)
        if result == 0:
            return function, "Error Communication BMS PING_COMMAND"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied PING_COMMAND"

        self.bmsPingTick = int.from_bytes(result[3:7], "big")

        timeNow = time.time()
        while time.time()-timeNow < 11:
            result = self.tenma.getVoltage()
            if result < 1:
                break
            if time.time()-timeNow > 10:
                if result > 0.5:
                    return function, "Mosfet Partially ON while off " + str(result)

        return function , "PASS " + "D: " + str(self.bmsPingTick)

    def pingFCT(self):
        function = "pingFCT"
        data = [FCTMacro.BMS_CELLCOUNT,0,0,0]
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.PING_FCT, data=data)
        if result == 0:
            return function, "Error Communication FCT Board PING_FCT"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied PING_FCT"
        elif result[3] != data[0]:
            return function, "BMS Cell Count Mismatch"
        return function, "PASS"

    def norFlashEEPROM(self):
        function = "norFlashEEPROM"
        result = self.canBus.sendCommandBMS(FCTMacro.NORFLASH_EEPROM_VERIFY, timeout=5)
        if result == 0:
            return function, "Error Communication BMS"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied"
        elif result[2] == FCTMacro.TEST_FAIL:
            return function, "Failed"
        return function, "PASS"

    def flashECUID(self):
        function = "flashECUID"
        tempID1 = self.ID1[2:]
        data = [0,0,0,0]
        for i in range(4):
            data[i] = int(tempID1[i*2:(i+1)*2], base=16)

        result = self.canBus.sendCommandBMS(FCTMacro.FLASH_ECUID_1_LSB, data=data)
        if result == 0:
            return function, "Error Communication BMS FLASH_ECUID_1_LSB "
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied FLASH_ECUID_1_LSB"
        tempID1 = self.ID2[2:]
        data = [0,0,0,0]
        for i in range(4):
            data[i] = int(tempID1[i*2:(i+1)*2], base=16)
        result = self.canBus.sendCommandBMS(FCTMacro.FLASH_ECUID_2_MSB, data=data)
        if result == 0:
            return function, "Error Communication BMS FLASH_ECUID_2_MSB "
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied FLASH_ECUID_2_MSB"

        return function, "PASS " + self.ID1[2:].upper() + " " + self.ID2[2:].upper() 

    def packFusePinRelayOff(self):
        function = "packFusePinRelayOff"
        data = [0,0,0,0]
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.PACK_FUSE_SENSE_MODE, data=data)
        if result == 0:
            return function, "Error Communication FCT Board PACK_FUSE_SENSE_MODE"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied PACK_FUSE_SENSE_MODE"
        return function, "PASS"

    def packFusePin(self):
        function = "packFusePin"
        data = [1,0,0,0]
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.PACK_FUSE_SENSE_MODE, data=data)
        
        if result == 0:
            return function, "Error Communication FCT Board PACK_FUSE_SENSE_MODE"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied PACK_FUSE_SENSE_MODE"
        
        result = self.canBus.sendCommandBMS(FCTMacro.PACK_FUSE_PIN_VALUE)
        
        if result == 0:
            return function, "Error Communication BMS PACK_FUSE_PIN_VALUE"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied PACK_FUSE_PIN_VALUE"
        if result[3] == 0:
            return function, "Failed as pin low"
        
        return function, "PASS"

    def initAFEs(self):
        function = "initAFEs"
        
        if FCTMacro.BMS_CELLCOUNT > 16:
            data=[2,0,0,0]
        else:
            data=[1,0,0,0]
        if FCTMacro.PDI_MODE !=3:
            result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.AFE_POGO_PIN_STATE, data=data)
            if result == 0:
                return function, "Error Communication FCT Board AFE_POGO_PIN_STATE"
            elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
                return function, "Request Denied AFE_POGO_PIN_STATE"
            elif result[3]==0:
                return function, "Test Failed AFE 1 OFF"
            elif FCTMacro.BMS_CELLCOUNT > 16:
                if result[4]==0:
                    return function, "Test Failed AFE 2 OFF"
        
        result = self.canBus.sendCommandBMS(FCTMacro.Init_AFE, timeout=10, tries=1)
        if result == 0:
            return function, "Error Communication BMS"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied"
        
        timeNow = time.time()
        while time.time()-timeNow < 11:
            result = self.tenma.getVoltage()
            if result < 1:
                break
            if time.time()-timeNow > 10:
                if result > 0.5:
                    return function, "Mosfet Partially ON while off " + str(result)

        return function, "PASS"

    def defaultModeFCT(self):
        function = "defaultModeFCT"
        result = self.tenma.setLoadCurrent(value=0)
        if result != 0:
            if result == 1:
                return function, "Failed to set Tenma value"
            elif result == 2:
                return function, "Failed to set Tenma mode"
            elif result == 3:
                return function, "Failed to set Tenma ON"

        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.DEFAULT_RELAT_MODE)
        if result == 0:
            return function, "Error Communication FCT Board DEFAULT_RELAT_MODE"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied DEFAULT_RELAT_MODE"
        
        return function, "PASS"
        
    def dsgMosfet(self):
        function = "dsgMosfet"

        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.Load_DSG)
        if result == 0:
            return function, "Error Communication FCT Board Load_DSG"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied Load_DSG"
        
        result = self.canBus.sendCommandBMS(FCTMacro.DSG_FET_ON)
        if result == 0:
            return function, "Error Communication BMS DSG_FET_ON"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied DSG_FET_ON"
        
        result = self.canBus.sendCommandBMS(FCTMacro.CHG_FET_ON)
        if result == 0:
            return function, "Error Communication BMS CHG_FET_ON"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied CHG_FET_ON"
        
        # time.sleep(1)

        # result = self.tenma.getVoltage()
        # if abs(result-FCTMacro.BMS_CELLCOUNT * 3) > 1:
        #     return function, "Mosfet Partially ON " + str(result)
        
        timeNow = time.time()
        while time.time()-timeNow < 6:
            result = self.tenma.getVoltage()
            if abs(result-FCTMacro.BMS_CELLCOUNT *(FCTMacro.EMULATOR_Voltage/1000)) < 1:
                break
            if time.time()-timeNow > 5:
                if result == 0:
                    return function, "Dsg Mosfet OFF " + str(result)
                elif abs(result-(FCTMacro.BMS_CELLCOUNT * (FCTMacro.EMULATOR_Voltage/1000))) > 0.5:
                    return function, "Mosfet Partially ON " + str(result)

        result = self.tenma.setLoadCurrent(value=int((FCTMacro.BMS_CELLCOUNT*(FCTMacro.EMULATOR_Voltage/1000))/FCTMacro.LOAD_CURRENT_DEFAULT), mode = "CR")
        if result != 0:
            if result == 1:
                return function, "Failed to set Tenma value"
            elif result == 2:
                return function, "Failed to set Tenma mode"
            elif result == 3:
                return function, "Failed to set Tenma ON"
        
        # result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.CHG_DSG_PIN)
        # if result == 0:
        #     return function, "Error Communication FCT Board CHG_DSG_PIN"
        # elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
        #     return function, "Request Denied CHG_DSG_PIN"
        # elif result[3] == 0 or result[4] == 0:
        #     return function, "Failed to turn Mosfet ON " + str(result[3]) + " " + str(result[4])
        
        timeNow = time.time()
        while time.time()-timeNow > 3:
            result = self.tenma.read_FlowCurr()
            if abs(result-FCTMacro.LOAD_CURRENT_DEFAULT) < FCTMacro.CURRENT_ACCURACY:
                break
            if time.time()-timeNow > 2:
                if abs(result-FCTMacro.LOAD_CURRENT_DEFAULT) > FCTMacro.CURRENT_ACCURACY:
                    return (function, "Failed During Current Flow " + str(result))
                break

        # result = self.tenma.read_FlowCurr()
        # if abs(result-FCTMacro.LOAD_CURRENT_DEFAULT) > FCTMacro.CURRENT_ACCURACY:
        #     return (function, "Failed During Current Flow " + str(result))
        
        result = self.canBus.sendCommandBMS(FCTMacro.DSG_FET_OFF_DFET)
        if result == 0:
            return function, "Error Communication BMS DSG_FET_OFF_DFET"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied DSG_FET_OFF_DFET"

        result = self.canBus.sendCommandBMS(FCTMacro.CHG_FET_OFF_CFET)
        if result == 0:
            return function, "Error Communication BMS CHG_FET_OFF_CFET"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied CHG_FET_OFF_CFET"

        # result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.CHG_DSG_PIN)
        # if result == 0:
        #     return function, "Error Communication FCT Board CHG_DSG_PIN"
        # elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
        #     return function, "Request Denied CHG_DSG_PIN"
        # elif result[3] == 0 or result[4] == 0:
        #     return function, "Failed to turn Mosfet OFF " + str(result[3]) + " " + str(result[4])

        # time.sleep(2)
        # result = self.tenma.read_FlowCurr()
        # if abs(result) > FCTMacro.CURRENT_ACCURACY:
        #     return (function, "Failed During Current Stop Load " + str(result)) 
        
        # return function, "PASS"

        timeNow = time.time()
        while time.time()-timeNow < 6:
            result = self.tenma.getVoltage()
            if result < 1:
                break
            if time.time()-timeNow > 5:
                if result > 0.5:
                    return function, "Mosfet Partially ON while off " + str(result)

        timeNow = time.time()
        while time.time()-timeNow < 2:
            result = self.tenma.read_FlowCurr()
            if result < FCTMacro.CURRENT_ACCURACY:
                return function, "PASS"

        return (function, "Failed During Current Stop Load " + str(result))
         
    def AFEShutdownAndHOrLDWake(self):
        function = "AFEShutdownAndHOrLDWake"

        timeNow = time.time()
        while time.time()-timeNow < 11:
            result = self.tenma.getVoltage()
            if result < 1:
                break
            if time.time()-timeNow > 10:
                if result > 0.5:
                    return function, "Mosfet Partially ON while off " + str(result)

        result = self.canBus.sendCommandBMS(FCTMacro.BUCK_PIN_OFF_MCU)
        if result == 0:
            return function, "Error Communication BMS BUCK_PIN_ON_MCU"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied BUCK_PIN_ON_MCU"

        result = self.canBus.sendCommandBMS(FCTMacro.AFE_SHUTDOWN)
        if result == 0:
            return function, "Error Communication BMS CHG_FET_OFF_CFET"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied CHG_FET_OFF_CFET"
        
        if FCTMacro.BMS_CELLCOUNT > 16:
            data=[2,0,0,0]
        else:
            data=[1,0,0,0]

        # time.sleep(5)
        # result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.AFE_POGO_PIN_STATE, data=data)
        # if result == 0:
        #     return function, "Error Communication FCT Board AFE_POGO_PIN_STATE"
        # elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
        #     return function, "Request Denied AFE_POGO_PIN_STATE"
        # elif result[3]==1:
        #     return function, "Test Failed AFE 1 ON"
        # elif FCTMacro.BMS_CELLCOUNT > 16:
        #     if result[4]==1:
        #         return function, "Test Failed AFE 2 ON"

        timeNow=time.time()    
        while time.time()-timeNow < 6:
            result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.AFE_POGO_PIN_STATE, data=data)
            if result == 0:
                return function, "Error Communication FCT Board AFE_POGO_PIN_STATE"
            elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
                return function, "Request Denied AFE_POGO_PIN_STATE"
            elif result[3]==0:
                if FCTMacro.BMS_CELLCOUNT > 16:
                    if result[4]== 0:
                        break
                break
            if time.time()-timeNow > 5:
                if result[3]==1:
                    return function, "Test Failed AFE 1 ON"
                elif FCTMacro.BMS_CELLCOUNT > 16:
                    if result[4]==1:
                        return function, "Test Failed AFE 2 ON"
            time.sleep(0.5)
            
            
        
        # result = self.testModule.sendCommandFCTBoard(self.ser,FCTMacro.READ_BUCK)
        # if result == 0:
        #     return function, "Error Communication FCT Board READ_BUCK"
        # elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
        #     return function, "Request Denied READ_BUCK"
        # elif abs(result[3])>20:
        #     return function, "TEST Failed 5v ON "+str(result[3])
        # elif abs(result[4])>20:
        #     return function, "TEST Failed 3.3v ON"+str(result[4])
        
        if FCTMacro.BMS_Version == 3:
            result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.H_WAKE)
            if result == 0:
                return function, "Error Communication FCT Board H_WAKE"
            elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
                return function, "Request Denied H_WAKE"
        elif FCTMacro.BMS_Version == 4:
            result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.Load_CHG)
            if result == 0:
                return function, "Error Communication FCT Board Load_CHG"
            elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
                return function, "Request Denied Load_CHG"
        
        # time.sleep(1)

        # result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.AFE_POGO_PIN_STATE, data=data)
        # if result == 0:
        #     return function, "Error Communication FCT Board AFE_POGO_PIN_STATE"
        # elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
        #     return function, "Request Denied AFE_POGO_PIN_STATE"
        # elif result[3]==0:
        #     return function, "Test Failed AFE 1 OFF"
        # elif FCTMacro.BMS_CELLCOUNT > 16:
        #     if result[4]==0:
        #         return function, "Test Failed AFE 2 OFF"

        timeNow=time.time()    
        while time.time()-timeNow < 6:
            result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.AFE_POGO_PIN_STATE, data=data)
            if result == 0:
                return function, "Error Communication FCT Board AFE_POGO_PIN_STATE"
            elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
                return function, "Request Denied AFE_POGO_PIN_STATE"
            elif result[3]==1:
                if FCTMacro.BMS_CELLCOUNT > 16:
                    if result[4]== 1:
                        break
                break
            if time.time()-timeNow > 5:
                if result[3]==0:
                    return function, "Test Failed AFE 1 OFF"
                elif FCTMacro.BMS_CELLCOUNT > 16:
                    if result[4]==0:
                        return function, "Test Failed AFE 2 OFF"
            time.sleep(0.5)
        
        # result = self.testModule.sendCommandFCTBoard(self.ser,FCTMacro.READ_BUCK)
        # if result == 0:
        #     return function, "Error Communication FCT Board READ_BUCK"
        # elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
        #     return function, "Request Denied READ_BUCK"
        # elif abs(result[3]-30)>1:
        #     return function, "TEST Failed 5v OFF "+str(result[3])
        # elif abs(result[4]-30)>1:
        #     return function, "TEST Failed 3.3v OFF"+str(result[4])
        
        return function, "PASS"

    def AFEShutdownAndLWake(self):
        function = "AFEShutdownAndLWake"
        timeNow = time.time()
        while time.time()-timeNow < 11:
            result = self.tenma.getVoltage()
            if result < 1:
                break
            if time.time()-timeNow > 10:
                if result > 0.5:
                    return function, "Mosfet Partially ON while off " + str(result)

        result = self.canBus.sendCommandBMS(FCTMacro.BUCK_PIN_OFF_MCU)
        if result == 0:
            return function, "Error Communication BMS BUCK_PIN_ON_MCU"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied BUCK_PIN_ON_MCU"

        result = self.canBus.sendCommandBMS(FCTMacro.AFE_SHUTDOWN)
        if result == 0:
            return function, "Error Communication BMS CHG_FET_OFF_CFET"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied CHG_FET_OFF_CFET"
        
        if FCTMacro.BMS_CELLCOUNT > 16:
            data=[2,0,0,0]
        else:
            data=[1,0,0,0]

        # time.sleep(5)

        # result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.AFE_POGO_PIN_STATE, data=data, timeout = 10)
        # if result == 0:
        #     return function, "Error Communication FCT Board AFE_POGO_PIN_STATE"
        # elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
        #     return function, "Request Denied AFE_POGO_PIN_STATE"
        # elif result[3]==1:
        #     return function, "Test Failed AFE 1 ON"
        # elif FCTMacro.BMS_CELLCOUNT > 16:
        #     if result[4]==1:
        #         return function, "Test Failed AFE 2 ON"

        timeNow=time.time()    
        while time.time()-timeNow < 6:
            result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.AFE_POGO_PIN_STATE, data=data)
            if result == 0:
                return function, "Error Communication FCT Board AFE_POGO_PIN_STATE"
            elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
                return function, "Request Denied AFE_POGO_PIN_STATE"
            elif result[3]==0:
                if FCTMacro.BMS_CELLCOUNT > 16:
                    if result[4]== 0:
                        break
                break
            if time.time()-timeNow > 5:
                if result[3]==1:
                    return function, "Test Failed AFE 1 ON"
                elif FCTMacro.BMS_CELLCOUNT > 16:
                    if result[4]==1:
                        return function, "Test Failed AFE 2 ON"
            time.sleep(0.5)
        
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.L_WAKE)
        if result == 0:
            return function, "Error Communication FCT Board L_WAKE"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied L_WAKE"
        
        # time.sleep(1)

        # result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.AFE_POGO_PIN_STATE, data=data)
        # if result == 0:
        #     return function, "Error Communication FCT Board AFE_POGO_PIN_STATE"
        # elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
        #     return function, "Request Denied AFE_POGO_PIN_STATE"
        # elif result[3]==0:
        #     return function, "Test Failed AFE 1 OFF"
        # elif FCTMacro.BMS_CELLCOUNT > 16:
        #     if result[4]==1:
        #         return function, "Test Failed AFE 2 ON"

        timeNow=time.time()    
        while time.time()-timeNow < 6:
            result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.AFE_POGO_PIN_STATE, data=data)
            if result == 0:
                return function, "Error Communication FCT Board AFE_POGO_PIN_STATE"
            elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
                return function, "Request Denied AFE_POGO_PIN_STATE"
            elif result[3]==1:
                if FCTMacro.BMS_CELLCOUNT > 16:
                    if result[4]== 1:
                        break
                break
            if time.time()-timeNow > 5:
                if result[3]==0:
                    return function, "Test Failed AFE 1 OFF"
                elif FCTMacro.BMS_CELLCOUNT > 16:
                    if result[4]==0:
                        return function, "Test Failed AFE 2 OFF"
            time.sleep(0.5)

        return function, "PASS" 

    def AFEShutdownAndHWakeMCU(self):
        function ="AFEShutdownAndHWakeMCU"
        # if FCTMacro.TestMode:

        timeNow = time.time()
        while time.time()-timeNow < 11:
            result = self.tenma.getVoltage()
            if result < 1:
                break
            if time.time()-timeNow > 10:
                if result > 0.5:
                    return function, "Mosfet Partially ON while off " + str(result)

        result = self.canBus.sendCommandBMS(FCTMacro.BUCK_PIN_ON_MCU)
        if result == 0:
            return function, "Error Communication BMS BUCK_PIN_ON_MCU"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied BUCK_PIN_ON_MCU"

        result = self.canBus.sendCommandBMS(FCTMacro.AFE_SHUTDOWN)
        if result == 0:
            return function, "Error Communication BMS CHG_FET_OFF_CFET"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied CHG_FET_OFF_CFET"

        if FCTMacro.BMS_CELLCOUNT > 16:
            data=[2,0,0,0]
            # time.sleep(4)
        else:
            data=[1,0,0,0]

        # time.sleep(5)

        # result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.AFE_POGO_PIN_STATE, data=data)
        # if result == 0:
        #     return function, "Error Communication FCT Board AFE_POGO_PIN_STATE"
        # elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
        #     return function, "Request Denied AFE_POGO_PIN_STATE"
        # elif result[3]==1:
        #     return function, "Test Failed AFE 1 ON"
        # elif FCTMacro.BMS_CELLCOUNT > 16:
        #     if result[4]==1:
        #         return function, "Test Failed AFE 2 ON"

        timeNow=time.time()    
        while time.time()-timeNow < 6:
            result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.AFE_POGO_PIN_STATE, data=data)
            if result == 0:
                return function, "Error Communication FCT Board AFE_POGO_PIN_STATE"
            elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
                return function, "Request Denied AFE_POGO_PIN_STATE"
            elif result[3]==0:
                if FCTMacro.BMS_CELLCOUNT > 16:
                    if result[4]== 0:
                        break
                break
            if time.time()-timeNow > 5:
                if result[3]==1:
                    return function, "Test Failed AFE 1 ON"
                elif FCTMacro.BMS_CELLCOUNT > 16:
                    if result[4]==1:
                        return function, "Test Failed AFE 2 ON"
            time.sleep(0.5)

        result = self.canBus.sendCommandBMS(FCTMacro.H_WAKE_FROM_MCU)
        if result == 0:
            return function, "Error Communication BMS H_WAKE_FROM_MCU"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied H_WAKE_FROM_MCU"
        
        # time.sleep(2)

        # result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.AFE_POGO_PIN_STATE, data=data)
        # if result == 0:
        #     return function, "Error Communication FCT Board AFE_POGO_PIN_STATE"
        # elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
        #     return function, "Request Denied AFE_POGO_PIN_STATE"
        # elif result[3]==0:
        #     return function, "Test Failed AFE 1 OFF"
        # elif FCTMacro.BMS_CELLCOUNT > 16:
        #     if result[4]==0:
        #         return function, "Test Failed AFE 2 OFF"

        timeNow=time.time()    
        while time.time()-timeNow < 6:
            result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.AFE_POGO_PIN_STATE, data=data)
            if result == 0:
                return function, "Error Communication FCT Board AFE_POGO_PIN_STATE"
            elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
                return function, "Request Denied AFE_POGO_PIN_STATE"
            elif result[3]==1:
                if FCTMacro.BMS_CELLCOUNT > 16:
                    if result[4]== 1:
                        break
                break
            if time.time()-timeNow > 5:
                if result[3]==0:
                    return function, "Test Failed AFE 1 OFF"
                elif FCTMacro.BMS_CELLCOUNT > 16:
                    if result[4]==0:
                        return function, "Test Failed AFE 2 OFF"
            time.sleep(0.5)

        return function, "PASS"

    def currentCalibration(self):
        function = "currentCalibration"
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.CURRENT_CALIBRATION_MODE)
        if result == 0:
            return function, "Error Communication FCT Board CURRENT_CALIBRATION_MODE"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied CURRENT_CALIBRATION_MODE"

        result = self.tenma.setLoadCurrent(value=FCTMacro.CALIBRATION_CURRENT_1, delay=1)
        if result != 0:
            if result == 1:
                return function, "Failed to set Tenma value CALIBRATION_CURRENT_1"
            elif result == 2:
                return function, "Failed to set Tenma mode"
            elif result == 3:
                return function, "Failed to set Tenma ON" 
        
        data = [FCTMacro.CALIBRATION_CURRENT_1,0,0,0]
        result = self.canBus.sendCommandBMS(FCTMacro.CALIBRATION_VALUE, data=data)
        
        if result == 0:
            return function, "Error Communication BMS CALIBRATION_VALUE"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied CALIBRATION_VALUE"
        
        result = self.tenma.setLoadCurrent(value=FCTMacro.CALIBRATION_CURRENT_2, delay=1)
        if result != 0:
            if result == 1:
                return function, "Failed to set Tenma value CALIBRATION_CURRENT_2"
            elif result == 2:
                return function, "Failed to set Tenma mode"
            elif result == 3:
                return function, "Failed to set Tenma ON" 

        data = [FCTMacro.CALIBRATION_CURRENT_2,0,0,0]
        result = self.canBus.sendCommandBMS(FCTMacro.CALIBRATION_VALUE, data=data)
        
        if result == 0:
            return function, "Error Communication BMS CALIBRATION_VALUE"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied CALIBRATION_VALUE"
        
        result = self.canBus.sendCommandBMS(FCTMacro.CALIBRATION_GAIN)
        if result == 0:
            return function, "Error Communication BMS CALIBRATION_GAIN"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied CALIBRATION_GAIN"
             
        self.currentGain = int.from_bytes(result[3:7], byteorder="little")

        result = self.canBus.sendCommandBMS(FCTMacro.CALIBRATION_OFFSET)
        if result == 0:
            return function, "Error Communication BMS CALIBRATION_OFFSET"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied CALIBRATION_OFFSET"
             
        self.currentOffset = int.from_bytes(result[3:7], byteorder="little", signed=True)

        if self.currentGain > 1200 or self.currentGain < 800:
            return function, "Current Gain OoB " + "G: " + str(self.currentGain) + " , O: " + str(self.currentOffset)

        return function, "PASS " + "G: " + str(self.currentGain) + " _ O: " + str(self.currentOffset)

    def daughterBoard(self):
        function = "daughterBoard"

        # result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.DAUGHTER_BOARD_5V)
        # if result == 0:
        #     return function, "Error Communication FCT Board DAUGHTER_BOARD_5V"
        # elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
        #     return function, "Request Denied DAUGHTER_BOARD_5V"
        # elif abs(result[3]-30)>5:
        #     return function, "TEST Failed 5v ON DAUGHTER "+str(result[3])
        
        result = self.canBus.sendCommandBMS(FCTMacro.PIN_HIGH_1650)
        if result == 0:
            return function, "Error Communication BMS PIN_HIGH_1650"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied PIN_HIGH_1650"
        
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.READ_1650_PIN)
        if result == 0:
            return function, "Error Communication FCT Board READ_1650_PIN"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied READ_1650_PIN"
        elif result[3] == 0 or result[4] == 0 or result[5] == 1 or result[6] == 0:
            return function, "Failed GPIO Daughter LOW"
        
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.READ_UART_PIN)
        if result == 0:
            return function, "Error Communication FCT Board READ_UART_PIN"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied READ_UART_PIN"
        elif result[3] == 0 or result[4] == 0 :
            return function, "Failed GPIO UART LOW"
        
        result = self.canBus.sendCommandBMS(FCTMacro.PIN_LOW_1650)
        if result == 0:
            return function, "Error Communication BMS PIN_LOW_1650"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied PIN_LOW_1650"
        
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.READ_1650_PIN)
        if result == 0:
            return function, "Error Communication FCT Board READ_1650_PIN"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied READ_1650_PIN"
        elif result[3] == 1 or result[4] == 1 or result[5] == 0 or result[6] == 1:
            return function, "Failed GPIO Daughter HIGH"
        
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.READ_UART_PIN)
        if result == 0:
            return function, "Error Communication FCT Board READ_UART_PIN"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied READ_UART_PIN"
        elif result[3] == 1 or result[4] == 1 :
            return function, "Failed GPIO UART HIGH"
        # ----------------------------------------------------
        
        return function, "PASS"

    def AFE_MCUDieTemp(self):
        function = "AFE_MCUDieTemp"
        result = self.canBus.sendCommandBMS(FCTMacro.AFE_DIE_TEMP_1)
        if result == 0:
            return function, "Error Communication BMS AFE_DIE_TEMP_1"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied AFE_DIE_TEMP_1"

        if FCTMacro.BMS_CELLCOUNT > 16:
            result = self.canBus.sendCommandBMS(FCTMacro.AFE_DIE_TEMP_2)
            if result == 0:
                return function, "Error Communication BMS AFE_DIE_TEMP_2"
            elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
                return function, "Request Denied AFE_DIE_TEMP_2" 
        
        result = self.canBus.sendCommandBMS(FCTMacro.MCU_DIE_TEMP)
        if result == 0:
            return function, "Error Communication BMS MCU_DIE_TEMP"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied MCU_DIE_TEMP" 

        return function, "PASS"

    def PackLDPinVoltage(self):
        function = "PackLDPinVoltage"
        data = [FCTMacro.SUPPLY_VOLTAGE, 0,0,0]
        result = self.canBus.sendCommandBMS(FCTMacro.PACK_LD_PIN_READ, data=data)
        if result == 0:
            return function, "Error Communication BMS PACK_LD_PIN_READ"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied PACK_LD_PIN_READ"
        
        packVoltage = int.from_bytes(result[3:5], byteorder="little")
        LDVoltage = int.from_bytes(result[5:7], byteorder="little")

        # print(packVoltage)
        # print(LDVoltage)
    
        return function , "PASS"

    def CANStandByPin(self):
        function = "CANStandByPin"
        result = self.canBus.sendCommandBMS(FCTMacro.WRITE_PIN_CAN_STDBY_HIGH)
        if result == 0:
            return function, "Error Communication BMS WRITE_PIN_CAN_STDBY_HIGH"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied WRITE_PIN_CAN_STDBY_HIGH" 
        
        if self.pingBMS()[1].find("PASS") != -1:
            return function, "FAILED BMS PING"

        return function, "PASS"

    def mcuFlash(self):
        function = "mcuFlash"
        result = self.flashCode(function, FCTMacro.BMS_APP_Code_location)
        if result[1] == "PASS":
           result = self.flashCode(function, FCTMacro.Bootloader_location)
        return result

    def CodeFlash(self):
        function = "CodeFlash"
        result = self.flashCode(function, FCTMacro.CodeFlash_location)
        return result

    def mcuFlashCustomer(self):
        function = "mcuFlashCustomer"
        result = self.flashCode(function, FCTMacro.BMS_CUSTOMER_CODE)
        return result

    def bmsDataValidation(self):
        function = "bmsDataValidation"
        refNtc = 0
        if FCTMacro.PDI_MODE != 1:
            result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.READ_REF_NTC)
            if result == 0:
                return function, "Error Communication FCT Board READ_REF_NTC"
            elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
                return function, "Request Denied READ_REF_NTC"
            refNtc = result[3]-127

            result = self.tenma.setLoadCurrent(value=0)
            if result != 0:
                if result == 1:
                    return function, "Failed to set Tenma value"
                elif result == 2:
                    return function, "Failed to set Tenma mode"
                elif result == 3:
                    return function, "Failed to set Tenma ON"
        
            result = self.canBus.canDataValidation(refNTC=refNtc, test=0)
            if result != 1:
                return function, "Precharge Fail"
        
            result = self.tenma.setLoadCurrent(value=int((FCTMacro.BMS_CELLCOUNT*(FCTMacro.EMULATOR_Voltage/1000))/FCTMacro.LOAD_CURRENT_DEFAULT), mode = "CR")
            if result != 0:
                if result == 1:
                    return function, "Failed to set Tenma value"
                elif result == 2:
                    return function, "Failed to set Tenma mode"
                elif result == 3:
                    return function, "Failed to set Tenma ON"

        result = self.canBus.canDataValidation(refNTC=refNtc)
        if result != 1:
            if result != 0:
                result = result - 3
                refArray = FCTMacro.createRefArray()
                return function, "Accuracy Error EC: " + refArray[result][0]
            else:
                return function, "Failed to get data"
            # print(result)
            # if result >=0 and result < FCTMacro.BMS_CELLCOUNT:
            #     return function, "Cell Voltage Accuracy Error EC: " + str(result)
            # elif result >=FCTMacro.BMS_CELLCOUNT and result < (FCTMacro.BMS_CELLCOUNT+4):
            #     return function, "NTC Accuracy Error EC: " + str(result)
            # elif result == FCTMacro.BMS_CELLCOUNT+FCTMacro.THERM_COUNT+2:
            #     return function, "Current Accuracy Error EC: " + str(result)
        return function, "PASS"
    
    def buckAccuracy(self):
        function = "buckAccuracy"
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.READ_BUCK)
        
        if result == 0:
            return function, "Error Communication FCT Board READ_BUCK"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied READ_BUCK"
        elif abs(result[3]-30)>3:
            return function, "TEST Failed 5v ON "+str(result[3])
        elif abs(result[4]-30)>3:
            return function, "TEST Failed 3.3v ON"+str(result[4])

        return function, "PASS"

    def emulatorOFF(self):
        functionName = "emulatorOFF"
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.EMULATOR_OFF)
        if result == 0:
            return function, "Error Communication FCT Board EMULATOR_OFF"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied EMULATOR_OFF"
        return functionName, "PASS"

    def bootMode(self):
        function ="bootMode"
        pktStartTime = time.time()
        result = self.canBus.sendCommandBMS(FCTMacro.BOOT_MODE)
        if result == 0:
            return function, "Error Communication with BMS Board"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied bootMode"
        self.ser.baudrate = 74880
        recData = "waiting for download"
        bRecData = bytes(recData, encoding='utf-8')
        recData2 = "Disabling RNG early entropy source..."
        bRecData2 = bytes(recData2, encoding='utf-8')
        recData3 = "No bootable app partitions in the partition table"
        bRecData3 = bytes(recData3, encoding='utf-8')
        while time.time() - pktStartTime < 10:
            rxData = self.ser.readline()
            print(rxData)
            if (str(rxData).find(bRecData.decode()) != -1):
                print("Boot Test Passed")
                return function, "PASS"
            if ((str(rxData).find(bRecData2.decode()) != -1) or (str(rxData).find(bRecData3.decode()) != -1)):
                print("Boot Mode Not Enable")
                return function, "Boot Mode Not Enabled"
            # break
        return function, "Failed ESP Not Responding"

    def ESPflashCode(self):
        function = "ESPflashCode"
        # pktStartTime = time.time()
        # self.ser.baudrate = 74880
        # while (time.time() - pktStartTime) <= 20:
        try: 
            self.ser.close()
            command = ['--port', self.serialPort,'--baud', '460800', "--before", "default_reset", "--after", "hard_reset", "--chip", "esp32c2", "write_flash", "--flash_mode", "dio", "--flash_freq", "60m", "--flash_size", "2MB", "0x0", "../Final/ESPCodes/bootloader.bin", "0x10000", "../Final/ESPCodes/BLEcodeFinal.bin", "0x8000", "../Final/ESPCodes/partition-table.bin"]
            print('Using command %s' % ' '.join(command))
            print(f"value: {esptool.main(command)}")
            self.ser.open()
            return function, "PASS"
        except Exception as e:
            print(f"Exception: {e}")
            return function, "FAIL to Flash Code"
            # self.ser.close()
            # cmd = os.system("python -m esptool -p COM11 -b 460800 --before default_reset --after hard_reset --chip esp32c2 write_flash --flash_mode dio --flash_freq 60m --flash_size 2MB 0x0 ../Final/ESPCodes/bootloader.bin 0x10000 ../Final/ESPCodes/BLEcodeFinal.bin 0x8000 ../Final/ESPCodes/partition-table.bin")
            # print(cmd)
            # self.ser.open()
            # if cmd == 0:
            #     return function, "PASS"
            # else:
            #     return function, "FAIL to Flash Code"
    
    def heartBeat(self):
        function = "heartBeat"
        pktStartTime = time.time()
        lRXData = []
        resetPass = 0
        HardBlue = self.ID1[2:].upper()+self.ID2[2:].upper()
        # print(HardBlue)
        bufferHardBlue = HardBlue.encode("utf-8").hex()
        # print(bufferHardBlue)
        bufferHardBlue = bytes.fromhex(bufferHardBlue)
        sum = 16
        for i in range(len(bufferHardBlue)):
            sum = sum + bufferHardBlue[i]
            # print(bufferHardBlue_TX[i])
        # print(sum)
        payload = hex((0xFFFF - sum) + 0x01)
        bufferHardBlue_TX = "DD050010" + str(HardBlue.encode("utf-8").hex()) + str(payload[2:]) + "77" 
        # print(bufferHardBlue_TX1)
        # bufferHardBlue_TX = "DD0500083031323334353637FE5C77"
        hexHardBlue_TX = bytes.fromhex(bufferHardBlue_TX)
        result = self.canBus.sendCommandBMS(FCTMacro.RESET_MODE)
        if result == 0:
            return function, "Error Communication with BMS Board"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied heartBeat"
        self.ser.baudrate = 74880
        recData2 = "Disabling RNG early entropy source..."
        bRecData2 = bytes(recData2, encoding='utf-8')
        while (time.time() - pktStartTime) <= 10:
            rxData = self.ser.readline()
            print(rxData)
            if (str(rxData).find(bRecData2.decode()) != -1):
                resetPass = 1
                self.ser.close()
                break
            elif ((time.time() - pktStartTime) >= 5):
                resetPass = 0
                self.ser.close()
                break

        self.ser.open()
        self.ser.baudrate  = 9600
        if (resetPass ==1):
             while (time.time() - pktStartTime) <= 10:
                rxData1 = self.ser.read()
                # print(rxData1)
                lRXData.append(rxData1)
                if(rxData1 == b'\xdd'):
                    # print("HeartBeat Start") 
                    continue
                if(rxData1 == b'w'):
                    # print("HeartBeat End")
                    print(lRXData)
                    break

             if lRXData == FCTMacro.heartBeat:
                print("HeartBeat Found")
                self.ser.write(hexHardBlue_TX)
                print(hexHardBlue_TX.hex())
                return function, "PASS"
             else:
                print("HeartBeat Not Found")
                return function,"HeartBeat Not Found, Test Failed"  
        else:
            return function, "RESET Not Done"
   
    def AudioVisual(self):
        self.dbUiUpdateCall.emit("1")
        function = "AudioVisual"
        timeNow = time.time()
        num = 1
        numericVal = 88
        buzValue = 1
        while time.time()-timeNow < 30:
            if num == 0:
                data = [0,0,0,0]
                num = 1
            else:
                data = [numericVal,buzValue,0,0]
                num = 0
            time.sleep(0.25)
            result = self.canBus.sendCommandBMS(FCTMacro.BUZZER_7SEGMENT, data = data)
            if result == 0:
                return function, "Error Communication with BMS Board"
            elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
                return function, "Request Denied AudioVisual"
            print(FM.AudioVisualStatus)
            if FM.AudioVisualStatus[0] == 1:
                self.buttonSelect.emit("buttonProceed")
            if FM.AudioVisualStatus[0] == 3:
                buzValue = 0
                numericVal = 44
                self.canBus.sendCommandBMS(FCTMacro.BUZZER_7SEGMENT, data = data)
            if FM.AudioVisualStatus[3] == 0:
                return function, "FAILED, 7 Segmant Display Not Soldered Properly"
            if FM.AudioVisualStatus[0] == 2:
                buzValue = 0
                numericVal = 0
                data = [numericVal,buzValue,0,0]
                self.canBus.sendCommandBMS(FCTMacro.BUZZER_7SEGMENT, data = data)
                if FM.AudioVisualStatus[1] == 1 and FM.AudioVisualStatus[2] == 1:
                    return function, "PASS"
                elif FM.AudioVisualStatus[1] == -1 or FM.AudioVisualStatus[2] == -1:
                    return function, "User did not responded to all values"
                else:
                    return function, "Failed " + str(FM.AudioVisualStatus[1]) + " " + str(FM.AudioVisualStatus[2]) + " " + str(FM.AudioVisualStatus[3])
        numericVal = 0
        buzValue = 0
        data = [numericVal,buzValue,0,0]
        self.canBus.sendCommandBMS(FCTMacro.BUZZER_7SEGMENT, data = data)
        return function, "Timeout Failed"
    
    """ Charger Runtime Functions """
    def Input_Ac_Supply(self):
        function = "Input_Ac_Supply"
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.AC_IN_SUPPLY_READ)
        rxCAN=self.canBus.readChargerCanData(FCTMacro.AC_IN_SUPPLY_CAN)

        # if result == 0 and rxCAN == 0:
        #     return function, "Error Communication FCT Board AC_IN_SUPPY_READ"
        # elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
        #     return function, "Request Denied AC_IN_SUPPLY_READ"
        
        input_ac=(result[3]<<24)|(result[4]<<16)|(result[5]<<8)|(result[6])
        # print(hex(result[3]))
        # print(hex(result[4]))
        # print(hex(result[5]))
        # print(hex(result[6]))
        # print(rxCAN)
        #if(input_ac>=27000):
        if (input_ac-27000) in range (200000,240000) or rxCAN in range (200,240):
            return function,str(input_ac-27000) +"_"+ str(rxCAN) +"_"+"PASS"
        else:
            return function,str(input_ac-27000)+"_"+str(rxCAN) +"_"+ "FAIL"
        #else:
            #input_ac=0
        #    return function,str(input_ac)+"_"+str(rxCAN) +"_"+ "FAIL"
        
    def Voltage_Point_12V(self):

        function = "Votlage_Point_12V"
        time.sleep(0.9)
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.POS_12V_READ)

        if result == 0:
            #  print("12V result:- ",result)
             return function, "Error Communication FCT Board POS_12V_READ"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied POS_12V_READ"
        pos_12v=( result[3] <<8 )|(result[4])
        # print(pos_12v)
        # print(result[3])
        # print(result[4])
        # pos_12v=int(((result[3]<<8)|0xffff)|result[4])
        # pos_12v=int(((8<<pos_12v)&0xFF00)|(result[4]))

        if pos_12v in range (11000,13500):
            return function,str(pos_12v) +"_"+ "PASS"
        else: 
            return function,str(pos_12v) +"_"+ "Issue In Pogo Pin" +"_"+ "PASS"
    
    def Voltage_Point_5V(self):

        function = "Votlage_Point_5V"
        time.sleep(0.9)
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.POS_5V_READ)

        if result == 0:
            return function, "Error Communication FCT Board POS_5V_READ"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied POS_5V_READ"
        
        pos_5v=(result[3]<<8)|(result[4])

        if pos_5v in range (4500,5500):
            return function,str(pos_5v) +"_"+ "PASS"
        else:
            return function,str(pos_5v) +"_"+ "Issue In Pogo Pin" +"_"+ "PASS"
        
    def Charger_Wait_Time(self,waitTime=20):

        function = "Charger_Wait_Time"
        startTime=time.time()
        while(time.time()-startTime<waitTime):
            pass
        return function,str(time.time()-startTime) +"_"+ "PASS"
        # # result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.POS_5V_READ)

        # # if result == 0:
        # #     return function, "Error Communication FCT Board POS_5V_READ"
        # # elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
        # #     return function, "Request Denied POS_5V_READ"
        
        # # pos_5v=(8<<result[3])|(result[4])

        # if pos_5v in range (4500,5500):
        #     return function, str(pos_5v) ,"PASS"
        # else:
        #     return function, "Voltage Point 5V Not In Range :"+str(pos_5v)
            
    def Terminal_Current(self):

        function = "Terminal_Current"
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.TRML_CRT_READ)
        rxCAN=self.canBus.readChargerCanData(FCTMacro.TRML_CRT_CAN)

        if result == 0 and rxCAN == 0:
            return function, "Error Communication FCT Board TRML_CRT_READ"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied TRML_CRT_READ"
        
        trml_crt=(result[3]<<8)|(result[4])
        if trml_crt in range (4500,5500) or rxCAN in range (45,55):
            return function,str(trml_crt) +"_"+ str(rxCAN) +"_"+ "PASS"
        else:
            return function,str(trml_crt)+"_"+str(rxCAN)+"_"+ "FAIL"
    
    def Terminal_Voltage(self):

        function = "Terminal_Voltage"
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.TRML_VTG_READ)
        rxCAN=self.canBus.readChargerCanData(FCTMacro.TRML_VTG_CAN)

        if result == 0 and rxCAN == 0:
            return function, "Error Communication FCT Board TRML_VTG_READ"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied TRML_VTG_READ"
        
        trml_vtg=(result[3]<<8)|(result[4])

        if trml_vtg in range (19000,24000) and rxCAN in range (190,240) :
            return function,str(trml_vtg) +"_"+ str(rxCAN) +"_"+ "PASS"
        else:
            return function,str(trml_vtg)+"_"+str(rxCAN)+"_"+ "FAIL"

    def LLC_Trans_Temp(self):

        function = "LLC_Trans_Temp"
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.LLC_TRANS_TEMP_READ)
        rxCAN=self.canBus.readChargerCanData(FCTMacro.MAX_TEMP_CAN)

        if result == 0 and rxCAN == 0:
            return function, "Error Communication FCT Board LLC_TRANS_TEMP_READ"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied LLC_TRANS_TEMP_READ"
        
        trans_temp=(result[3])

        if trans_temp in range (15,35) or rxCAN in range (15,35):
            return function,str(trans_temp) +"_"+ str(rxCAN) +"_"+ "PASS"
        else:
            return function,str(trans_temp)+"_"+str(rxCAN)+"_"+ "FAIL"

    def LLC_Mosfet_Temp(self):
            
        function = "LLC_Mosfet_Temp"
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.LLC_MOS_TEMP_READ)
        rxCAN=self.canBus.readChargerCanData(FCTMacro.MAX_TEMP_CAN)

        if result == 0 and rxCAN == 0:
            return function, "Error Communication FCT Board LLC_MOS_TEMP_READ"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied LLC_MOS_TEMP_READ"
        
        mos_temp=(result[3])

        if mos_temp in range (15,35) or rxCAN in range (15,35):
            return function,str(mos_temp) +"_"+ str(rxCAN) +"_"+ "PASS"
        else:
            return function,str(mos_temp)+"_"+str(rxCAN)+"_"+ "FAIL"
            
    def LLC_Diode_Temp(self):
            
        function = "LLC_Diode_Temp"
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.LLC_DIODE_TEMP_READ)
        rxCAN=self.canBus.readChargerCanData(FCTMacro.MAX_TEMP_CAN)

        if result == 0 and rxCAN == 0:
            return function, "Error Communication FCT Board LLC_DIODE_TEMP_READ"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied LLC_DIODE_TEMP_READ"
        
        diode_temp=(result[3])

        if diode_temp in range (15,35) or rxCAN in range (15,35):
            return function,str(diode_temp) +"_"+ str(rxCAN) +"_"+ "PASS"
        else:
            return function,str(diode_temp)+"_"+str(rxCAN)+"_"+ "FAIL"
            
    def PFC_Mosfet_Temp(self):
            
        function = "PFC_Mosfet_Temp"
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.PFC_MOS_TEMP_READ)
        rxCAN=self.canBus.readChargerCanData(FCTMacro.MAX_TEMP_CAN)

        if result == 0  and rxCAN == 0:
            return function, "Error Communication FCT Board PFC_MOS_TEMP_READ"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied PFC_MOS_TEMP_READ"
        
        pfc_mos_temp=(result[3])

        if pfc_mos_temp in range (15,35) or rxCAN in range (15,35):
            return function,str(pfc_mos_temp) +"_"+ str(rxCAN) +"_"+ "PASS"
        else:
            return function,str(pfc_mos_temp)+"_"+str(rxCAN)+"_"+ "FAIL"
        
    def CHGR_FCT_RelayON(self):

        function="CHGR_FCT_RelayON"
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.TURN_ON_CHARGER)

        if result == 0:
            # print("Relay Result:- ",result)
            return function, "Error Communication FCT Board TURN_ON_CHARGER"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied TURN_ON_CHARGER"
        
        return function,"PASS"
        

    def CHGR_FCT_RelayOFF(self):

        function="CHGR_FCT_RelayOFF"
        result = self.testModule.sendCommandFCTBoard(self.ser, FCTMacro.TURN_OFF_CHARGER)

        if result == 0:
            return function, "Error Communication FCT Board TURN_OFF_CHARGER"
        elif result[2] == FCTMacro.REQUEST_DENIED or result[2] == FCTMacro.TEST_FAIL:
            return function, "Request Denied TURN_OFF_CHARGER"
        ###
        time.sleep(1)
        return function,"PASS"

        
    def ChargerTestFlash(self):

        function = "ChargerTestFlash"
        result=self.flashCode(function,FCTMacro.Charger_Test_Code_Loc, FCTMacro.CH_TESTCODE_FLASH_ADDR)
        return result
    
    def ChargerFinalFlash(self):

        function = "ChargerFinalFlash"
        result=self.flashCode(function,FCTMacro.Charger_Final_Code_Loc,FCTMacro.CH_CUTOMER_FLASH_ADDR)
        return result
    
    def ChargerBootloaderFlash(self):

        function = "ChargerFinalFlash"
        result=self.flashBootloaderCode(function,FCTMacro.Charger_Bootloader_Loc,FCTMacro.CH_BOOTLOADER_FLASH_ADDR)
        return result

def unsecure_device_hook(title, msg, flags):
    return pylink.enums.JLinkFlags.DLG_BUTTON_YES
    



    # def bmsDataValidationPDI(self):
    #     function = "bmsDataValidationPDI"
        
    #     result = self.canBus.canDataValidation(test=0)
    #     # if result == 1:
    #     #     return function, "Precharge Fail Because it passed"

    #     result = self.canBus.canDataValidation()
    #     if result != 1:
    #         result = result - 3
    #         # print(result)
    #         if result >=0 and result < FCTMacro.BMS_CELLCOUNT:
    #             return function, "Cell Voltage Accuracy Error EC: " + str(result)
    #         elif result >=FCTMacro.BMS_CELLCOUNT and result < (FCTMacro.BMS_CELLCOUNT+4):
    #             return function, "NTC Accuracy Error EC: " + str(result)
    #         elif result == FCTMacro.BMS_CELLCOUNT+FCTMacro.THERM_COUNT+2:
    #             return function, "Current Accuracy Error EC: " + str(result)
    #     bmsId1 = bmsId2 = ""
    #     # ECUID = self.canBus.getECUID()
    #     # try: 
    #     #     print(ECUID)
    #     #     bmsId1=hex(ECUID[0]).upper()
    #     #     bmsId2=hex(ECUID[1]).upper()
    #     #     pdiId1=self.ID1.upper()
    #     #     pdiId2=self.ID2.upper()

    #     #     # print(bmsId1[-7:])
    #     #     Id2Len = len(bmsId2)
    #     #     # print(Id2Len)
    #     #     if Id2Len < 10:
    #     #         zero = "00000000"
    #     #         bmsId2 = bmsId2[2:]
    #     #         bmsId2 = zero[0:10-Id2Len]+bmsId2
    #     #         bmsId2 = "0x" + bmsId2
    #     #     # print(bmsId2[-7:])
    #     #     # print(pdiId1[-7:])
    #     #     # print(pdiId2[-7:])
    #     #     if ECUID != None:
    #     #         if bmsId1[-6:] != pdiId1[-6:] or bmsId2[-6:] != pdiId2[-6:]:
    #     #             return function, "Ecuid Failed " + hex(ECUID[0]) + " " + hex(ECUID[1])
    #     #     else:
    #     #         return function, "Ecuid Rx Failed"
    #     # except Exception as e:
    #     #     print(str(e)
    #     if self.canBus.canBus:
    #         self.canBus.canBus.shutdown()
    #     return function, "PASS ECUID: " + bmsId1 + ", " + bmsId2


    # def initTestSetupPDI(self):
    #     function = "initTestSetupPDI"
        
    #     result = self.canBus.connectPort()
    #     if result != 1:
    #         return ("BMS CAN:","No CAN device found, Error Code: " +str(result))

    #     return function, "PASS"   


     # while 1:
            #     i=1

            # result = self.initTestSetup()
            # if self.outputError(result, flag=True):
            #     return None
            
            # result = self.pingFCT()
            # self.outputError(result)   
            
            # result = self.chargeMosfetSC()
            # self.outputError(result)

            # # res = self.defaultModeFCT()
            # # FCTMacro.errorLogger(test=res[0],msg=res[1])
            
            # result = self.emulatorON()
            # self.outputError(result)

            # result = self.flashTestcode()
            # if self.outputError(result, flag=True):
            #     return None 

            # result = self.pingBMS()
            # self.outputError(result) 
            
            # result = self.norFlashEEPROM()
            # self.outputError(result) 
            
            # result = self.flashECUID()
            # self.outputError(result) 
            
            # result = self.packFusePin()
            # self.outputError(result) 
            
            # res = self.packFusePinRelayOff()
            # FCTMacro.errorLogger(test=res[0],msg=res[1])
            
            # result = self.AFEShutdownAndHWake()
            # self.outputError(result)
            # # if self.outputError(result, flag=True):
            # #     return None
            
            # result = self.AFEShutdownAndLWake()
            # self.outputError(result) 
            # # if self.outputError(result, flag=True):
            # #     return None
            
            # result = self.AFEShutdownAndHWakeMCU() # uncomment fct code on fct board testing
            # self.outputError(result)
            # # if self.outputError(result, flag=True):
            # #     return None 
        
            # time.sleep(5)

            # result = self.initAFEs()
            # self.outputError(result) 
                        
            # result = self.currentCalibration()
            # self.outputError(result) 

            # # res = self.defaultModeFCT()
            # # FCTMacro.errorLogger(test=res[0],msg=res[1])

            # result = self.dsgMosfet()
            # self.outputError(result) 
            
            # result = self.daughterBoard()
            # self.outputError(result) 
            
            # # result = self.AFE_MCUDieTemp() // Not Needed
            # # self.outputError(result) 
            
            # # result = self.PackLDPinVoltage() // Not Needed
            # # self.outputError(result) 

            # result = self.CANStandByPin()
            # self.outputError(result)
            
            # result = self.mcuFlash()
            # if self.outputError(result, flag=True):
            #     return None  
            
            # result = self.bmsDataValidation()
            # self.outputError(result) 
            
            # result = self.buckAccuracy()#self.mcuFlashJ1939()#
            # self.outputError(result)  

            # res = self.defaultModeFCT()
            # FCTMacro.errorLogger(test=res[0],msg=res[1]) 

            # if self.failCount > 0:
            #     self.outputError(("BMS Test Failed","Failed Test Count: " + str(self.failCount) + " Test Time: " + str(self.TotalTestTime) + "s"),flag=True)
            # else:
            #     self.outputError(("BMS Test Passed","PASS" + " Test Time: " + str(self.TotalTestTime)),flag=True)
