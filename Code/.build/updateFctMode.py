import os
import json
import shutil

Totalmodes = 2

mode = ["FCT", "PDI"]

fileName = "fctMode.py"

def getVersion():
    f = open('package.json')
    data = json.load(f)
    f.close()
    s=data["version"]
    s= "v" + s.replace(".", "_")
    return s

def getCurrentMode():
    f = open(fileName, "r")
    s=f.readline()
    f.close()
    return s

def modeUpdate():
    s=getCurrentMode()
    for i in range(Totalmodes):
        if str(i) in s:
            if i == Totalmodes-1:
                s=s.replace(str(i),str(0))
            else:
                s=s.replace(str(i),str(i+1))
            print(s)
            os.system("pyinstaller --onefile --noconsole main.py")
            fileNameExe = mode[i] + getVersion() + ".exe"
            os.system("ren .\dist\main.exe " + fileNameExe)
            s2 = "MOVE .\dist\\" + fileNameExe + " ..\\20s_Production\\"
            os.system(s2)
            shutil.rmtree("build")
            shutil.rmtree("dist")
            os.remove("main.spec")
            break
    os.remove(fileName)
    f = open(fileName, "w")
    # towrite = "ver = \"" + getVersion() +"\""
    # print(towrite)
    f.write(s)
    f.close()

modeUpdate()