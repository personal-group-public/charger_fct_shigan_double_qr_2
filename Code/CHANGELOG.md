

## [2.2.0](https://gitlab.com/Vecmocon_Technologies/experimental_test_bench_s32k11x/compare/1.1.0...2.2.0) (2023-09-11)


### Features

* release version as task added ([ad8bc8b](https://gitlab.com/Vecmocon_Technologies/experimental_test_bench_s32k11x/commit/ad8bc8baee062c663bd585a88f0b44a743b94c84))


### Minor tasks

* init version changed ([0f35442](https://gitlab.com/Vecmocon_Technologies/experimental_test_bench_s32k11x/commit/0f3544205f5eb1550abe6a150572fe514a9b7da7))

## [1.1.0](https://gitlab.com/Vecmocon_Technologies/experimental_test_bench_s32k11x/compare/FCT_20s_v2_1...1.1.0) (2023-09-11)


### Features

* switch can baudrate function ([5e17535](https://gitlab.com/Vecmocon_Technologies/experimental_test_bench_s32k11x/commit/5e175352502c779f1ba470e961e6b142b3a53a09))