from base64 import decode
from tracemalloc import start
from typing_extensions import Self
import serial
import serial.tools.list_ports
import cantools
import can
import can.interfaces.serial.serial_can
import j1939
from canlib import kvadblib
import time
import FCTMacro as FM
FCTMacro = FM.FCTMacro()


class canCommunication():
    def __init__(self, dbcFileName, ecuid):
        self.PID = FCTMacro.CAN_BUS_PID
        self.VID = FCTMacro.CAN_BUS_VID
        # self.baudrate = 115200
        # self.dbcFile = dbcFileName
        self.dbc = cantools.database.load_file(dbcFileName)
        if(FCTMacro.PDI_MODE == 5):
            self.ecuidBms = int(ecuid,17)
        else:
            self.ecuidBms = int(ecuid,16)
        self.comPorts = self.__getPortList(serial.tools.list_ports.comports())
        # self.refDataArray       = FCTMacro.REF_VALUES_ARRAY()
        # self.keyArray           = FCTMacro.STDCAN_KEYS()

        self.canReadCount       = 0         
        self.testType           = 0
        self.can_data_2         = {}

        self.id_lut = {}
        self.metaData={}
        
        self.getECUIDMeta = 0
        self.J1939result = 2

    def __getPortList(self, allPorts):
        portList = []
        for port in allPorts:
            if port.pid == self.PID and port.vid == self.VID:
                portList.append(port.device)

        return portList

    def connectPort(self):
        result = ""
        if(len(self.comPorts) > 0):
            for port in self.comPorts:
                try:
                    canBus = can.interface.Bus(port, bustype='serial')
                    FCTMacro.errorLogger(test="CAN Module Found", msg=str(port))
                    # print("CAN Module Found ", end="")
                    # print(port)
                    del canBus
                    return 1
                except Exception as e:
                    result = result + str(e)
                    continue
        return result

    def sendCommandBMS(self, command, data=[0,0,0,0], timeout=FCTMacro.DEFAULT_REQUEST_TIMEOUT, tries = 3):
        try:
            dataToSend = [0] * 8
            dataToSend[0] = ord('$')
            dataToSend[1] = FCTMacro.PC_ID
            dataToSend[2] = command
            for i in range(4):
                dataToSend[3+i]=data[i]
            dataToSend[7] = ord('#')
            retries = 0
            try:
                can_bus = self.__connectPort()
            except Exception as e:
                print(f"Exception: {e}")
                return 0
            while(retries < tries):
                msg = can_bus.recv(0.01)
                startTime = time.time()
                packet = can.Message(is_extended_id = False, arbitration_id = FCTMacro.CAN_ID, data = dataToSend)
                can_bus.send(packet)
                # dataRecv = 0
                while True:
                    currTime = time.time()
                    msg = can_bus.recv(0.01)
                    if(msg):
                        intTemp = [0]*8
                        for i in range(8):
                            intTemp[i] = int(msg.data[i])
                        FCTMacro.errorLogger(test="sendCommandBMS: "+str(command), msg=intTemp)
                        if msg.data[1] != command:
                            del can_bus
                            return 0
                        else: 
                            del can_bus
                            return msg.data                    
                    if (currTime - startTime) >= timeout:
                        print(dataToSend)
                        retries += 1
                        break
            del can_bus
            return 0
        except Exception as e:
            FCTMacro.errorLogger(test="sendCommandBMS exception", msg=str(e))
            del can_bus
            return 0

    def __connectPort(self):
        if(len(self.comPorts) > 0):
            for port in self.comPorts:
                try:
                    ser = can.interface.Bus(port, bustype='serial')
                    ser.recv(0.01)
                    # packet = can.Message(is_extended_id = False, arbitration_id = 0x54, data = [0, 0, 0, 0, 0x80, 0, 0, 0])
                    # ser.send(packet)
                    return ser
                except Exception as e:
                    FCTMacro.errorLogger(test="CAN BUS Connect port exception", msg=str(e))
                    continue
        else:
            FCTMacro.errorLogger(test="CAN BUS Connect port", msg="No CAN device found")
            return -1
    

    def checkData(self):
        for i in range(len(self.RefArray)):
            if self.dataArray[i] == "":
                print(self.dataArray)
                return 2

        self.canReadCount += 1
        FCTMacro.errorLogger(test="CAN BUS Data:", msg=self.dataArray)

        for i in range(len(self.RefArray)):
            if self.RefArray[i][0] == "MOSFETStatusPrecharge":
                if self.dataArray[i] == "PreChargingProgress":
                    self.canReadCount -= 1
                    return 2

        if self.testType == 0:
            for i in range(len(self.RefArray)):
                if self.RefArray[i][0] == "MOSFETStatusPrecharge":
                    if self.dataArray[i] == "PreChargeSuccessful":
                        return 1
                    elif self.dataArray[i] == "PreChargeFailed" or self.canReadCount>3:
                        return 3
                    # elif self.dataArray[i] == "PreChargingProgress":
                    #     self.canReadCount -= 1
                    #     return 2
            return 0

        for i in range(len(self.RefArray)):
            if self.RefArray[i][2] == -1:
                continue
            elif self.RefArray[i][2]==0:
                if self.RefArray[i][1] != self.dataArray[i]:
                    print(f"{self.RefArray[i][1]}-{self.dataArray[i]}")
                    self.errorArray[i] += 1
            elif abs(self.RefArray[i][1]-(self.dataArray[i])) > self.RefArray[i][2]:
                    print(f"{self.RefArray[i][1]}-{self.dataArray[i]}")
                    self.errorArray[i] += 1

        for i in range(len(self.RefArray)):
            if self.errorArray[i] > FCTMacro.CAN_READ_COUNT/2:
                FCTMacro.errorLogger(test="CAN BUS Error:", msg=self.errorArray)
                return (3 + i)
        if self.canReadCount>FCTMacro.CAN_READ_COUNT:
                return 1
        return 2
    
    def decodeCANData(self):
        if "Precharge_Temp" in self.can_data_2.keys():
            CD = ""
            for item in self.can_data_2.keys():
                try:
                    upperLetter=[char for char in item if char.isupper() or char in {"0","1","2","3","4","5","6","7","8","9"}]
                    # print(upperLetter)
                    s=""
                    s1=s.join(upperLetter)
                    if self.can_data_2[item] == None:
                        continue
                    CD += "%s"%s1
                    CD += "="
                    CD += "%s"%(self.can_data_2[item])
                    CD += ", "
                except:
                    pass
            FCTMacro.errorLogger(test="CAN Data:", msg=CD)
            for i in range(len(self.RefArray)):
                if self.RefArray[i][0] in self.can_data_2.keys():
                    self.dataArray[i] = self.can_data_2[self.RefArray[i][0]]
            del self.can_data_2["Precharge_Temp"]
            return self.checkData() 
        else:
            return 2

    def on_message(self,timestamp,pgn, data, stdID = False):
        can_data = {}
        # global keys
        global pdstr
        global pCan_data
        ldata = []
        for i in range(0, len(data)):
            ldata.append(int(data[i]))
        if len(ldata) > 0:
            if(stdID == False):
                id = (6 << 26) | (pgn << 8) | (0x23)
            else:
                id = pgn

            can_data = self.dbc.decode_message(id, ldata)
            # for key in keys:
            for key in can_data:
                self.can_data_2[key] = can_data[key]
            return self.decodeCANData()

    def canDataValidation(self, refNTC=0, test=1):
        self.testType = test
        if FCTMacro.PDI_MODE == 1 or (self.testType == 0 and FCTMacro.PDI_MODE != 1):
            self.RefArray = FCTMacro.createRefArray()
            self.dataArray          = [""] * len(self.RefArray)
            self.errorArray         = [0] * (len(self.RefArray))
            for i in range(len(self.RefArray)):
                if refNTC != 0:
                    if self.RefArray[i][0] in ["MOSFET_Temp", "Precharge_Temp"]:
                        self.RefArray[i][1] = refNTC
                if self.RefArray[i][0] == "ECUSerialNumber_Part1":
                    self.RefArray[i][1] = self.ecuidBms
            self.errorArray         = [0] * (len(self.RefArray))
            self.canReadCount       = 0 
            FCTMacro.errorLogger(test="CAN BUS Key:", msg=self.RefArray)

            self.switchBaudRate(FCTMacro.CAN_BAUDRATE)

        # can_bus = self.__connectPort()
        # for i in range(20):
        #     msg = can_bus.recv(0.1)
            # if msg:
            #     print(msg.data)
        # timeNow = time.time()
        # while time.time()-timeNow < 5:
        #     msg = can_bus.recv(0)
        #     if(msg):
        #         if(msg.arbitration_id > 0x7FF):
        #             self.protocolCAN = "J1939"
        # del can_bus

        

        if FCTMacro.CAN_PROTOCOL == "STDCAN":
            result = self.STDCANProtocol()
        elif FCTMacro.CAN_PROTOCOL == "J1939":
            result = self.J1939Protocol()
        print(f"result: {result}, {self.canReadCount}")
        return result

    def getECUID(self, can_bus):
        startTime = time.time()
        while((time.time() - startTime) < 30):
            msg = can_bus.recv(1)
            if(msg):
                if(int(msg.arbitration_id) not in [176]):
                    packet = can.Message(is_extended_id = False, arbitration_id = 0x54, data = [0, 0, 0, 0, 0, 0, 0, 0])
                    # print("Sending BMS_Disable >>")
                    can_bus.send(packet)
                    packet = can.Message(is_extended_id = False, arbitration_id = 0x356, data = [0, 0, 0, 0, 0, 0, 0, 0])
                    # print("Sending BMS_Disable >>")
                    can_bus.send(packet)
                else:
                    decodedPacket = self.dbc.decode_message(msg.arbitration_id,msg.data)
                    print(decodedPacket)
                    for key in decodedPacket:
                        self.can_data_2[key] = decodedPacket[key]
                    return 1
            else:
                packet = can.Message(is_extended_id = False, arbitration_id = 0x54, data = [0, 0, 0, 0, 0x80, 0, 0, 0])
                # print("Sending BMS_Enable")
                can_bus.send(packet)
        return None

    def STDCANProtocol(self):
        can_bus = self.__connectPort()
        timeNow = time.time()
        start2 = time.time()
        if FCTMacro.PDI_MODE == 1 or (self.testType == 0 and FCTMacro.PDI_MODE != 1):
            if self.getECUID(can_bus) == None:
                return 0
        while (time.time()-timeNow) < 40:
            now = time.time()
            if(((now - start2) > 1)):
                packet = can.Message(is_extended_id = False, arbitration_id = 0x54, data = [0, 0, 0, 0, 0x80, 0, 0, 0])
                can_bus.send(packet)
                start2 = now
            msg = can_bus.recv(0)
            try:
                if(msg):
                    # print(msg.data)
                    result = self.on_message(msg.timestamp,msg.arbitration_id,msg.data,True)
                    if result != 2:
                        del can_bus
                        return result
            except Exception as e:
                print(f"canDataValidation: {e}")
        del can_bus
        return 0
    
    def init_dbc(self):
        rx = 'BMS_App'
        keys = []
        self.id_lut.clear()
        for message in self.dbc.messages:
            if message.name!='BMS_MetaData' and message.name!='BMS_ECUID' and message.name!='SoftwareIdentification' and message.name!='NetID':
                if message.name=='BatteryCellVoltages':
                    if rx in message.senders:
                        for signal in message.signals:
                            if self.metaData['SeriesConfiguration']<18 and ('StacknCellVoltage' in str(signal)):
                                tempstr = str(signal.name)
                                if int(tempstr[18:])<=self.metaData['SeriesConfiguration']:
                                    keys.append(signal.name)
                            else:
                                keys.append(signal.name)
                                
                elif message.name=='FaultStatus':
                    if rx in message.senders:
                        for signal in message.signals:
                            if ('StacknBalancingStatus_Cell' in str(signal)):
                                tempstr = str(signal.name)
                                if int(tempstr[26:])<=self.metaData['SeriesConfiguration']:
                                    keys.append(signal.name)
                            else:
                                keys.append(signal.name)

                elif message.name=='BatteryTemperatures':
                    if rx in message.senders:
                        for signal in message.signals:
                            if ('StacknTemperature' in str(signal)):
                                tempstr = str(signal.name)
                                if int(tempstr[18:])<=self.metaData['TemperatureSensorCount']:
                                    keys.append(signal.name)
                            else:
                                keys.append(signal.name)
                else:
                    if rx in message.senders:
                        for signal in message.signals:
                            keys.append(signal.name)
            if message.frame_id > 0x7FF:
                mid = j1939.MessageId(can_id=message.frame_id)
                pgn = j1939.ParameterGroupNumber()
                pgn.from_message_id(mid)
                self.id_lut[pgn.value] = message.frame_id
            else:
                self.id_lut[message.frame_id] = message.frame_id
        print(self.id_lut)
        # for i in keys:
        #     can_data[i] = None
        
    def on_messageJ1939(self, pgn, data):
        # if FCTMacro.printRawCAN:
        #     print(data)
        try:
            global StackVoltage_Calculated
            ldata = []
            # print(pgn)
            for i in range(0, len(data)):
                ldata.append(int(data[i]))
            if len(ldata) > 0:
                if pgn in self.id_lut:
                    id = self.id_lut[pgn]
            
                    if pgn in [64965, 61847, 65242, 65295]:
                        meta_data = self.dbc.decode_message(id, ldata)
                        if FCTMacro.printRawCAN:
                            print(meta_data)
                        for key in meta_data:
                            self.metaData[key] = meta_data[key]
                        if "ECUSerialNumber_Part1" in self.metaData.keys() :
                            self.getECUIDMeta = 4
                            #print(self.metaData)
                            # self.metaData['ECUPN_Part1'] = hex(self.metaData['ECUPN_Part1'])
                            # self.metaData['ECUPN_Part2'] = hex(self.metaData['ECUPN_Part2'])
                            # self.metaData['ECUPN_Part3'] = hex(self.metaData['ECUPN_Part3'])
                            # self.metaData['Delimiter_1'] = hex(self.metaData['Delimiter_1'])
                            # self.metaData['ECUSerialNumber_Part1'] = hex(self.metaData['ECUSerialNumber_Part1'])
                            # self.metaData['ECUSerialNumber_Part2'] = hex(self.metaData['ECUSerialNumber_Part2'])
                            # self.metaData['Delimiter_3'] = hex(self.metaData['Delimiter_3'])
                            # self.metaData['ECULocation'] = hex(self.metaData['ECULocation'])
                            # self.metaData['Delimiter_4'] = hex(self.metaData['Delimiter_4'])
                            # self.metaData['ECUType'] = hex(self.metaData['ECUType'])
                            # self.metaData['Delimiter_2'] = hex(self.metaData['Delimiter_2'])
                            FCTMacro.errorLogger(test="CAN Meta Data:", msg=self.metaData)
                            for i in range(len(self.RefArray)):
                                if self.RefArray[i][0] in self.metaData.keys():
                                    self.dataArray[i] = self.metaData[self.RefArray[i][0]]
                            # self.can_data_2 = {**self.can_data_2, **self.metaData}
                            self.init_dbc()   
                    else:
                        if self.metaData!={}:
                            try:
                                if id == 0x14fffc23:
                                    return
                                print_data = self.dbc.decode_message(id, ldata)
                                if self.metaData['SeriesConfiguration']>18 and id==419415843 and print_data['StackIndex_CellVoltages']==3:
                                    for i in range(19, self.metaData['SeriesConfiguration']+1,1):
                                        self.can_data_2['StacknCellVoltage_'+str(i)] = print_data['StacknCellVoltage_'+(str(i-18))]
                                else:
                                    # for key in keys:
                                    for key in print_data:
                                        self.can_data_2[key] = print_data[key]
                                if id==419415843:
                                    if print_data['StackIndex_CellVoltages']==2:
                                        StackVoltage_Calculated=0
                                        for i in range(1, 17,1):
                                            StackVoltage_Calculated += print_data['StacknCellVoltage_'+(str(i))]

                                    if print_data['StackIndex_CellVoltages']==3:
                                        for i in range(19, self.metaData['SeriesConfiguration']+1,1):
                                            StackVoltage_Calculated += print_data['StacknCellVoltage_'+(str(i-18))]
                                if id == 419419171:
                                    balancingtriggerresponse = print_data['Rsvd_1']
                                # print(self.can_data_2)
                                res ={**self.can_data_2, **self.metaData}
                            except Exception as e:
                                print(f"Exception: {e}")
                                pass
        except Exception as e:
            print(f"Exception: {e}")                

    def j1939_sync(self, client):
            sync_packet_1 = can.Message(is_extended_id=True, arbitration_id = 0x14ef2324, data = [16,0,0,0,0,0,0,0])
            client.send(sync_packet_1)
            time.sleep(0.005)
            sync_packet_fup = can.Message(is_extended_id=True, arbitration_id = 0x14ef2324, data = [24,0,0,0,0,0,0,0])
            client.send(sync_packet_fup)

    def recv_data(self, client):
            start2=0
            try:
                msg = client.recv(0.1)
                if msg == None:
                    self.heartBeatCount += 1
                    return
                self.heartBeatCount = 0
                if(msg.arbitration_id == 0x14fffc23):
                    print(msg)
                    if((msg.data[0]) != 1):
                        self.j1939_sync(client)
                        return
                    if self.getECUIDMeta==0:
                        # MetaPacket = can.Message(is_extended_id=True, arbitration_id = 0x18ff8d69, data = [0,0,0,0,0,0,0,0])      #Balancing off ideal Message
                        # client.send(MetaPacket)

                        MetaPacket = can.Message(is_extended_id=True, arbitration_id = 0x00ea2324, data = [151,241,0])
                        client.send(MetaPacket)

                        MetaPacket = can.Message(is_extended_id=True, arbitration_id = 0x00ea2324, data = [197,253,0])      #ECUID
                        client.send(MetaPacket)
                        print("send ecuid request packet")
                        self.getECUIDMeta=1
                    elif self.getECUIDMeta==1:
                        MetaPacket = can.Message(is_extended_id=True, arbitration_id = 0x00ea2324, data = [15,255,0])      #NETID    
                        client.send(MetaPacket)
                        self.getECUIDMeta=2
                    
                    elif self.getECUIDMeta==2:
                        MetaPacket = can.Message(is_extended_id=True, arbitration_id = 0x00ea2324, data = [218,254,0])      #Software Identification
                        client.send(MetaPacket)
                        self.getECUIDMeta=0

                if(msg.arbitration_id > 0x7ff):
                    self.ecu.notify(msg.arbitration_id, msg.data, time.time())
            except Exception as e: 
                print(f"Exception: {e}")
                
    def init_Config(self):
        keys = []
        rx = 'BMS_App'
        if len(self.id_lut) != 0:
            return
        self.ecu = j1939.ElectronicControlUnit()
        for message in self.dbc.messages:
            if message.name=='BMS_MetaData' or message.name=='BMS_ECUID' or message.name=='SoftwareIdentification' or message.name=='NetID':
                if rx in message.senders:
                    for signal in message.signals:
                        keys.append(signal.name)
                if message.frame_id > 0x7FF:
                    mid = j1939.MessageId(can_id=message.frame_id)
                    pgn = j1939.ParameterGroupNumber()
                    pgn.from_message_id(mid)
                    self.id_lut[pgn.value] = message.frame_id
                else:
                    self.id_lut[message.frame_id] = message.frame_id
        print(self.id_lut)
        # for i in keys:
        #     can_data[i] = None
    
    def J1939Protocol(self):
        self.init_Config()
        self.ecu.subscribe(self.on_messageJ1939)
        timeNow = time.time()
        client = self.__connectPort()
        self.heartBeatCount = 0
        while (time.time()-timeNow) < 40:
            self.recv_data(client)
            if self.heartBeatCount > 40:
                print("Reconnecting CAN Bus..")
                self.heartBeatCount = 0
                del client
                client = self.__connectPort()
            self.J1939result = self.decodeCANData()
            if self.J1939result !=2:
                self.ecu.stop()
                del client
                return self.J1939result
        return 0
    
    def switchBaudRate(self,baudRate):
        CMD_ACK = 0x79
        CMD_BAUDRATE=0x33
        BaudrateDict = {
			"500" : 6,
			"250" : 12
		}
		# self.DATA_BAUDRATE_500_0=6
		# self.DATA_BAUDRATE_250_0=12
        canBus = self.__connectPort()
        baudRateValue=[BaudrateDict[baudRate],0x79,0X00,0X00,0,0,0,0]
        packet = can.Message(is_extended_id = True, arbitration_id = CMD_BAUDRATE, data = baudRateValue)
        canBus.send(packet, timeout=0.1)
        timeNow = time.time()
        while time.time() - timeNow < 0.5:
            msg = canBus.recv(0.1)
            # if msg and msg.dlc > 0:
            #     print(msg)
            if msg and msg.arbitration_id == CMD_BAUDRATE and msg.dlc == 1:
                if msg.data[0] == CMD_ACK:
                    FCTMacro.errorLogger(test=f"Switch Baudrate: {baudRate}", msg="Passed")
                    del canBus
                    return 1
        FCTMacro.errorLogger(test=f"Switch Baudrate: {baudRate}", msg="Failed")
        del canBus
        return 0
    
    def readChargerCanData(self,datatype):
        try:
            can_bus = self.__connectPort()
        except Exception as e:
            print(f"Exception: {e}")
            return 0
        startTime=time.time()
        tries=0
        while(tries < 3):
            try:
                diag_msg=can_bus.recv(0.5)
                if(diag_msg.data[0]== 36 and diag_msg.arbitration_id==FCTMacro.CHARGER_DIAG_MESSAGE_CAN_ID):
                    match datatype:
                        case FCTMacro.AC_IN_SUPPLY_CAN:
                                del can_bus
                                return diag_msg.data[1]
                        case FCTMacro.TRML_CRT_CAN:
                                del can_bus
                                return diag_msg.data[2]
                        case FCTMacro.TRML_VTG_CAN:
                                del can_bus
                                return diag_msg.data[3]
                        case FCTMacro.MAX_TEMP_CAN:
                                del can_bus
                                return diag_msg.data[4]
                if(time.time() -startTime) <= (FCTMacro.DEFAULT_REQUEST_TIMEOUT):
                    tries=tries+1
            except Exception as e:
                print(f"Exception: {e}")
                return 0
        del can_bus
        return 0








