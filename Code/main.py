# import support as s
import serial
import time
from datetime import datetime
import serial.tools.list_ports
import gui as gui
import sys
from tkinter.messagebox import askokcancel, showinfo, WARNING
import fctForm
import FCTMacro as FM
FCTMacro = FM.FCTMacro()


def window():

    app = gui.QtWidgets.QApplication(sys.argv)
    MainWindow = gui.QtWidgets.QMainWindow()
    ui = gui.Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    ui.startTest.clicked.connect(ui.changePage)
    # ui.setECUID.clicked.connect(ui.setBMSID)
    ui.nextTest.clicked.connect(ui.nextTestCall)
    # ui.endTest.clicked.connect(ui.close)
    # ui.retryButton.clicked.connect(ui.retryTestCall)
    ui.scanECUID.clicked.connect(ui.scanBMS_HID)

    # ui.retryButton.clicked.connect(True)
    sys.exit(app.exec_())

if __name__ == "__main__":
    # g = gui.debug()
    # fctForm.getDatabase()
    if FCTMacro.TestMode == 0 or FCTMacro.TestMode == 1:
        fctForm.fillFctForm()
    window()
