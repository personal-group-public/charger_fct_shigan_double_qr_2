python .\.build\updateVersion.py 
set /p Build=<latestVersion.py 
echo %Build%
git add latestVersion.py
git commit -m build:_%Build%_%TIME%

pyinstaller --distpath .. --noconfirm .\main.py

@REM set productionName=Charger_FCT_Shigan_14sNMC_BS
@REM set productionName=charger_fct_shigan_14snmc_bs_2
@REM set productionName=charger_fct_shigan_double_qr_1
     set productionName=charger_fct_shigan_double_qr_2
@REM set productionName=pdi_20s_okinawa_shigan
@REM set productionName=fct_20s_shigan_okinawa
@REM set productionName=db_okinaw_ravitele
@REM set productionName=Code_Flash_Tool
set productionPath=..\..\..\ProductionTools\
mkdir %productionPath%
git clone --depth 1 https://gitlab.com/personal-group-public/%productionName%.git %productionPath%%productionName%

xcopy ..\main %productionPath%%productionName%\main /Y /E /H
cd %productionPath%\%productionName%
git add .
git commit -m build:_%Build%_%TIME%
git push

@REM xcopy "C:\Users\deepa\Desktop\Vecmocon\Projects\Production\experimental_test_bench_s32k11x\Test_Bench_PY\main" "C:\Users\deepa\Desktop\Vecmocon\Projects\Production\ProductionTools\fct_20s_shigan_okinawa\main" /Y /E /H
@REM cd "C:\Users\deepa\Desktop\Vecmocon\Projects\Production\ProductionTools\fct_20s_shigan_okinawa"
@REM git add .
@REM git commit -m build:_%Build%_%TIME%
@REM git push

@REM xcopy "C:\Users\deepa\Desktop\Vecmocon\Projects\Production\experimental_test_bench_s32k11x\Test_Bench_PY\main" "C:\Users\deepa\Desktop\Vecmocon\Projects\Production\ProductionTools\db_okinaw_ravitele\main" /Y /E /H
@REM cd "C:\Users\deepa\Desktop\Vecmocon\Projects\Production\ProductionTools\db_okinaw_ravitele"
@REM git add .
@REM git commit -m build:_%Build%_%TIME%
@REM git push

@REM xcopy "C:\Users\deepa\Desktop\Vecmocon\Projects\Production\experimental_test_bench_s32k11x\Test_Bench_PY\main" "C:\Users\deepa\Desktop\Vecmocon\Projects\Production\ProductionTools\pdi_20s_okinawa_shigan\main" /Y /E /H
@REM cd "C:\Users\deepa\Desktop\Vecmocon\Projects\Production\ProductionTools\pdi_20s_okinawa_shigan"
@REM git add .
@REM git commit -m build:_%Build%_%TIME%
@REM git push

@REM xcopy "C:\Users\deepa\Desktop\Vecmocon\Projects\Production\experimental_test_bench_s32k11x\Test_Bench_PY\main" "C:\Users\deepa\Desktop\Vecmocon\Projects\Production\ProductionTools\Code_Flash_Tool\main" /Y /E /H
@REM cd "C:\Users\deepa\Desktop\Vecmocon\Projects\Production\ProductionTools\Code_Flash_Tool"
@REM git add .
@REM git commit -m build:_%Build%_%TIME%

@REM exit

@REM cd "C:\Users\deepa\Desktop\Vecmocon\Projects\Production\experimental_test_bench_s32k11x\Test_Bench_PY\Code"

@REM :error
@REM echo failed with error #%errorlevel%