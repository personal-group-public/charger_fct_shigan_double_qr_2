# import openpyxl and tkinter modules
from openpyxl import *
from tkinter import *
from tkinter import ttk
from datetime import datetime
from tkinter.messagebox import askokcancel, showinfo, WARNING
from tkinter import messagebox

import sys

import FCTMacro as FM
FCTMacro = FM.FCTMacro()

# globally declare wb and sheet variable
# opening the existing excel file
wb = load_workbook('../Final/Book1.xlsx', data_only=True)

# create the sheet object
sheet = wb["Sheet1"] # Logs Sheet
cusList=[]
hwList=[]
swList=[]

testingPassword = "123456789"
fixInfo = "02020202"
def getDatabase():

	try:
		global hwList
		global swList
		global cusList

		sheet1 = wb["HW"] # Hardware List sheet
		lastCol=sheet1.max_column
		lastRow=sheet1.max_row
		hwList=[""]*(lastRow-1)
		# print(lastCol)
		# print(lastRow)
		for y in range(2,lastRow+1):
			s=""
			for x in range(1,lastCol+1):
				value=sheet1.cell(row=y, column=x).value
				# print(value)
				if value != None:
					s = s + str(value) + "-"
					# print(s)
			hwList[y-2]=s
		# print(hwList)

		sheet1 = wb["SW"] # Software List sheet
		lastCol=sheet1.max_column
		lastRow=sheet1.max_row
		swList=[""]*(lastRow-1)
		# print(lastCol)
		# print(lastRow)
		for y in range(2,lastRow+1):
			s=""
			for x in range(1,lastCol+1):
				value=sheet1.cell(row=y, column=x).value
				# print(value)
				if value != None:
					s = s + str(value) + "-"
					# print(s)
			swList[y-2]=s
		# print(swList)

		sheet1 = wb["Customer"] # Customer List sheet
		lastCol=sheet1.max_column
		lastRow=sheet1.max_row
		cusList=[""]*(lastRow-1)
		# print(lastCol)
		# print(lastRow)
		for y in range(2,lastRow+1):
			s=""
			for x in range(1,lastCol+1):
				value=sheet1.cell(row=y, column=x).value
				# print(value)
				if value != None:
					s = s + str(value) + "-"
					# print(s)
			cusList[y-2]=s
		# print(cusList)

	except Exception as e:
		FCTMacro.errorLogger("Ecuid Not scanned yet", test="getDatabase", msg=str(e))


def excel():
	
	# resize the width of columns in
	# excel spreadsheet
	sheet.column_dimensions['A'].width = 30
	sheet.column_dimensions['B'].width = 50
	sheet.column_dimensions['C'].width = 5
	sheet.column_dimensions['D'].width = 5
	sheet.column_dimensions['E'].width = 5
	sheet.column_dimensions['F'].width = 5
	sheet.column_dimensions['G'].width = 5
	sheet.column_dimensions['H'].width = 20

	# write given data to an excel spreadsheet
	# at particular location
	sheet.cell(row=1, column=1).value = "TimeStamp"
	sheet.cell(row=1, column=2).value = "Unique ID"
	sheet.cell(row=1, column=3).value = "Extra 4"
	sheet.cell(row=1, column=4).value = "Extra 5"
	sheet.cell(row=1, column=5).value = "Extra 1"
	sheet.cell(row=1, column=6).value = "Extra 2"
	sheet.cell(row=1, column=7).value = "Extra 3"
	sheet.cell(row=1, column=8).value = "Verification Person"

# Function to set focus (cursor)
def focus1(event):
	# set focus on the course_field box
	course_field.focus_set()

# Function to set focus
def focus2(event):
	# set focus on the sem_field box
	sem_field.focus_set()

# Function to set focus
def focus3(event):
	# set focus on the form_no_field box
	form_no_field.focus_set()

# Function to set focus
def focus4(event):
	# set focus on the contact_no_field box
	contact_no_field.focus_set()

# Function to set focus
def focus5(event):
	# set focus on the email_id_field box
	email_id_field.focus_set()

# Function to set focus
def focus6(event):
	# set focus on the address_field box
	address_field.focus_set()

# Function for clearing the
# contents of text entry boxes
def clear():
	# clear the content of text entry box
	name_field.delete(0, END)
	# course_field.delete(0, END)
	# sem_field.delete(0, END)
	# form_no_field.delete(0, END)
	# contact_no_field.delete(0, END)
	# email_id_field.delete(0, END)
	address_field.delete(0, END)

# Function to take data from GUI
# window and write to an excel file
def insert():
	global name_field
	global course_field
	global sem_field
	global form_no_field
	global contact_no_field
	global email_id_field
	global address_field

	# if user not fill any entry
	# then print "empty input"
	if (name_field.get() == "" or
		# course_field.get() == "" or
		# sem_field.get() == "" or
		address_field.get() == ""):
		# print("empty input")
		messagebox.showerror('InputError', 'Error: One of the fields is empty')
	else:
		# assigning the max row and max column
		# value upto which data is written
		# in an excel sheet to the variable
		current_row = sheet.max_row
		current_column = sheet.max_column

		# get method returns current text
		# as string which we write into
		# excel spreadsheet at particular location
		sheet.cell(row=current_row + 1, column=1).value = datetime.now().strftime("%d-%m-%Y %H:%M:%S")
		sheet.cell(row=current_row + 1, column=2).value = name_field.get()
		# sheet.cell(row=current_row + 1, column=3).value = course_field.get()
		# sheet.cell(row=current_row + 1, column=4).value = sem_field.get()
		# sheet.cell(row=current_row + 1, column=5).value = form_no_field.get()
		# sheet.cell(row=current_row + 1, column=6).value = contact_no_field.get()
		# sheet.cell(row=current_row + 1, column=7).value = email_id_field.get()
		sheet.cell(row=current_row + 1, column=8).value = address_field.get()

		s = address_field.get()
		if len(s)<4:
			messagebox.showerror('CriticalError', 'Invalid ID >4, Contact Support For Help')
			# sys.exit()
			return

		s=name_field.get()
		try:
			value = int(s,16)
		except Exception as e:
			messagebox.showerror('CriticalError', str(e))
		
		id_length = 8
		if(FCTMacro.PDI_MODE == 5):
			id_length = 9

		if len(s) != id_length :
			messagebox.showerror('CriticalError', 'Invalid ID =8, Contact Support For Help')
			return
		elif value >0xFFFFFFFFFFFFFFFF:
			messagebox.showerror('CriticalError', 'Invalid value >, Contact Support For Help')
			return
		
		# index=s.index('-')
		# s=s[:index]
		# cusId=hex(int(s))
		cusId = s#cusId[2:]
		# if len(cusId)<2:
		# 	cusId='0'+cusId
		# if cusId == "ff" and address_field.get() == testingPassword:
		# 	cusId = "FF"
		# 	sheet.cell(row=current_row + 1, column=8).value = "********"
		# print(cusId)

		# s=course_field.get()
		# index=s.index('-')
		# s=s[:index]
		# hwId=hex(int(s))
		# hwId=hwId[2:]
		# if len(hwId)<2:
		# 	hwId='0'+hwId
		# print(hwId)

		# s=sem_field.get()
		# index=s.index('-')
		# s=s[:index]
		# swId=hex(int(s))
		# swId=swId[2:]
		# swId='0'*(4-len(swId))+swId
		# print(swId)
		global fixInfo
		fixInfo=cusId
		# print(fixInfo)

		# save the file
		wb.save('../Final/Book1.xlsx')
		# set focus on the name_field box
		name_field.focus_set()
		# call the clear() function
		clear()
		root.destroy()

		#goto window
		return 1


def check_input1(event):
    value = event.widget.get()
    if value == '':
        name_field['values'] = cusList
    else:
        data = []
        for item in cusList:
            if value.lower() in item.lower():
                data.append(item)
        name_field['values'] = data

def check_input2(event):
    value = event.widget.get()
    # if value == '':
    #     course_field['values'] = hwList
    # else:
    #     data = []
    #     for item in hwList:
    #         if value.lower() in item.lower():
    #             data.append(item)
    #     course_field['values'] = data

def check_input3(event):
    value = event.widget.get()
    if value == '':
        sem_field['values'] = swList
    else:
        data = []
        for item in swList:
            if value.lower() in item.lower():
                data.append(item)
        sem_field['values'] = data

# Driver code
# if __name__ == "__main__":
def fillFctForm():
	try:
		excel()

		# sheet1 = wb["Sheet1"] # Hardware List sheet
		# lastRow=sheet.max_row 
		# print(lastRow)
		# LastCustName=sheet.cell(lastRow,2).value
		# LastHWID=sheet.cell(lastRow,3).value
		# LastSWID=sheet.cell(lastRow,4).value
		# LastVerPerson=sheet.cell(lastRow,8).value

		# print(str(LastCustName) + str(LastHWID) + str(LastSWID) + str(LastVerPerson))


		# create a GUI window
		global root
		root = Tk()

		# set the background colour of GUI window
		root.configure(background='light green')

		# set the title of GUI window
		root.title("FCT Config Form")

		# set the configuration of GUI window
		root.geometry("600x200")

		# create a Form label
		heading = Label(root, text="Form", bg="light green")

		# create a Name label
		name = Label(root, text="Unique ID", bg="light green")

		# create a Course label
		course = Label(root, text="Extra 4", bg="light green")

		# create a Semester label
		sem = Label(root, text="Extra 5", bg="light green")

		# create a Form No. label
		form_no = Label(root, text="Extra 1", bg="light green")

		# create a Contact No. label
		contact_no = Label(root, text="Extra 2", bg="light green")

		# create a Email id label
		email_id = Label(root, text="Extra 3", bg="light green")

		# create a address label
		address = Label(root, text="Verification Person", bg="light green")

		# grid method is used for placing
		# the widgets at respective positions
		# in table like structure .
		heading.grid(row=0, column=1)
		name.grid(row=1, column=0)
		# course.grid(row=2, column=0)
		# sem.grid(row=3, column=0)
		# form_no.grid(row=4, column=0)
		# contact_no.grid(row=5, column=0)
		# email_id.grid(row=6, column=0)
		address.grid(row=4, column=0)
		global name_field
		global course_field
		global sem_field
		global form_no_field
		global contact_no_field
		global email_id_field
		global address_field
		# create a text entry box
		# for typing the information
		# combo_box = Entry(root)
		# course_field = Entry(root)
		# sem_field = Entry(root)
		form_no_field = Entry(root)
		contact_no_field = Entry(root)
		email_id_field = Entry(root)

		text = StringVar()
		# text.set(LastVerPerson)
		address_field = Entry(root,textvariable = text)



		# creating Combobox
		name_field = ttk.Combobox(root)
		global cusList
		name_field['values'] = cusList
		name_field.bind('<KeyRelease>', check_input1)
		# name_field.set(LastCustName)

		# course_field = ttk.Combobox(root)
		# global hwList
		# course_field['values'] = hwList
		# course_field.bind('<KeyRelease>', check_input2)
		# course_field.set(LastHWID)

		# sem_field = ttk.Combobox(root)
		# global swList
		# sem_field['values'] = swList
		# sem_field.bind('<KeyRelease>', check_input3)
		# sem_field.set(LastSWID)

		# combo_box.pack()

		# bind method of widget is used for
		# the binding the function with the events

		# whenever the enter key is pressed
		# then call the focus1 function
		name_field.bind("<Return>", focus1)

		# whenever the enter key is pressed
		# then call the focus2 function
		# course_field.bind("<Return>", focus2)

		# whenever the enter key is pressed
		# then call the focus3 function
		# sem_field.bind("<Return>", focus3)

		# whenever the enter key is pressed
		# then call the focus4 function
		# form_no_field.bind("<Return>", focus4)

		# whenever the enter key is pressed
		# then call the focus5 function
		# contact_no_field.bind("<Return>", focus5)

		# whenever the enter key is pressed
		# then call the focus6 function
		# email_id_field.bind("<Return>", focus6)

		# grid method is used for placing
		# the widgets at respective positions
		# in table like structure .
		name_field.grid(row=1, column=1, ipadx="100")
		# course_field.grid(row=2, column=1, ipadx="100")
		# sem_field.grid(row=3, column=1, ipadx="100")
		# form_no_field.grid(row=4, column=1, ipadx="100")
		# contact_no_field.grid(row=5, column=1, ipadx="100")
		# email_id_field.grid(row=6, column=1, ipadx="100")
		address_field.grid(row=4, column=1, ipadx="100")
		# combo_box.grid(row=11, column=1,ipadx="100")

		# call excel function
		# excel()

		# create a Submit Button and place into the root window
		submit = Button(root, text="Submit", fg="Black",bg="Red", command=insert)
		submit.grid(row=8, column=1)

		root.protocol("WM_DELETE_WINDOW", on_closing)

		# start the GUI
		root.mainloop()

	except Exception as e:
		root.protocol("WM_DELETE_WINDOW", on_Error)
		FCTMacro.errorLogger("Ecuid Not scanned yet", test="fillFCTForm", msg=str(e))

def on_closing():
    if askokcancel("Quit", "Do you want to quit?"):
        sys.exit()

def on_Error():
   messagebox.showerror('CriticalError', 'Please Restart the Software and try Again')