from datetime import datetime
import os.path
import json
import latestVersion

f = open("../../production_logs/errorLogs.txt","a")
AudioVisualStatus = [-1] * 4
class FCTMacro:
	def __init__(self):
		self.heartBeat = [b'\xdd', b'\xa6', b'\xbb', b'\x02', b'\x00', b'\x00', b'\xff', b'C', b'w']
		fconfig = open('../Final/config.json')
		dataConfig = json.load(fconfig)
		fconfig.close()
		self.BMS_CELLCOUNT = dataConfig["BMS_CELLCOUNT"]
		self.THERM_COUNT = 7
		self.CAN_PROTOCOL = dataConfig["CAN_PROTOCOL"] 
		self.CAN_BAUDRATE = dataConfig["CAN_BAUDRATE"]
		self.CustomerName = dataConfig["CustomerName"]
		self.printRawCAN = dataConfig["printRawCAN"]
		self.EMULATOR_Voltage = dataConfig["RefVoltage"]

		self.TestMode = dataConfig["TestMode"]

		self.Charger_Test_Code_Loc = dataConfig["Charger_Test_Code_Loc"]
		self.Charger_Final_Code_Loc = dataConfig["Charger_Final_Code_Loc"]
		self.Charger_Commit_ID = dataConfig["Charger_Commit_ID"] 

		self.CHARGER_SW_VERSION		  =	4.8
		self.CHARGER_HW_VERSION		  = 2.5
		self.CHARGER_LOGIC_HW_VERSION = 2.5
		
		self.BMS_Version = 4
		self.BMS_CUSTOMER_CODE = "../Final/Binary/" + self.CustomerName+ "_" + self.CAN_PROTOCOL + "_" + str(self.BMS_CELLCOUNT) + "_" + self.CAN_BAUDRATE + ".hex"
		self.BMS_APP_Code_location = "../Final/Binary/" + self.CustomerName+ "_" + self.CAN_PROTOCOL + "_" + str(self.BMS_CELLCOUNT) + "_" + self.CAN_BAUDRATE + "_test.hex"
		# 0 = FCT, 1 = PDI, 2 = CODEFLASH, 3 = NO FCT BOARD
		Mode = {
			"CF": 2,
			"PDI": 1,
			"FCT": 0,
			"DB": 4,
			"FCT_Charger": 5,
			"FCT16": 6
		}
		self.PDI_MODE = Mode[dataConfig["PDI_MODE"]]

		self.TEST_ARRAY_FCT = [
			"initTestSetup",
			"pingFCT", 
			"chargeMosfetSC", 
			"emulatorON", 
			"flashTestcode", 
			"pingBMS",
			"norFlashEEPROM", 
			"flashECUID",
			"initAFEs",
			"packFusePin",  
			"currentCalibration", 
			"dsgMosfet", 
			"daughterBoard", 
			"AFEShutdownAndLWake",
			"AFEShutdownAndHOrLDWake", 
			"AFEShutdownAndHWakeMCU", 
			"CANStandByPin", 
			"mcuFlash", 
			"bmsDataValidation", 
			"buckAccuracy", 
			"mcuFlashCustomer",
			"Result"
			]

		self.TEST_ARRAY_NO_FCT = [
			"initTestSetup",
			"flashTestcode", 
			"pingBMS",
			"initAFEs",
			"norFlashEEPROM", 
			"flashECUID", 
			"currentCalibration", 
			"CANStandByPin", 
			"mcuFlash", 
			"bmsDataValidation", 
			"Result"
			]

		self.TEST_ARRAY_PDI = [
			"initTestSetup",
			"bmsDataValidation", 
			"Result"
			]

		self.TEST_ARRAY_CODEFLASH = [
			"CodeFlash",
			"Result"
			]
		self.TEST_ARRAY_DAUGHTERBOARD = [
			"initTestSetup",
			"bootMode",
			"ESPflashCode",
			"heartBeat",
			"AudioVisual",
			"Result"
			]
		
		"""Charger Test Array"""

		# self.TEST_ARRAY_FCT_Charger = [
		# 	"initTestSetup",
		# 	"CHGR_FCT_RelayON",
		# 	"Voltage_Point_12V",
		# 	"Voltage_Point_5V",
		# 	"ChargerTestFlash",
		# 	"Charger_Wait_Time",
		# 	"Input_Ac_Supply", 
		# 	"LLC_Trans_Temp", 
		# 	"LLC_Mosfet_Temp", 
		# 	"LLC_Diode_Temp", 
		# 	"PFC_Mosfet_Temp",
		# 	"Terminal_Current", 
		# 	"Terminal_Voltage",
		# 	"ChargerFinalFlash",
		# 	"CHGR_FCT_RelayOFF", 
		# 	"Result"
		# 	]

		### Test array relay on off addded BEFORE TEST FIRMWARE FALSH

		self.TEST_ARRAY_FCT_Charger = [
			"initTestSetup",
			"CHGR_FCT_RelayON",
			"Voltage_Point_12V",
			"Voltage_Point_5V",
			"ChargerTestFlash",
			"CHGR_FCT_RelayOFF", 
			"CHGR_FCT_RelayON",
			"Charger_Wait_Time",
			"Input_Ac_Supply", 
			"LLC_Trans_Temp", 
			"LLC_Mosfet_Temp", 
			"LLC_Diode_Temp", 
			"PFC_Mosfet_Temp",
			"Terminal_Current", 
			"Terminal_Voltage",
			"ChargerBootloaderFlash",
			"ChargerFinalFlash",
			"CHGR_FCT_RelayOFF", 
			"Result"
			]
		
		
		"""-------END------"""

		self.LOAD_CURRENT_DEFAULT = dataConfig["LOAD_CURRENT_DEFAULT"]#In ampere
		self.CALIBRATION_CURRENT_1 = 6 #In ampere
		self.CALIBRATION_CURRENT_2 = 12 #In ampere
		self.CURRENT_ACCURACY =  (dataConfig["Current_Accuracy_percentage"] * self.LOAD_CURRENT_DEFAULT)#In ampere
		self.VOLTAGE_ACCURACY = dataConfig["VOLTAGE_ACCURACY"]  #in millivolt
		self.NTC_ACCURACY = 5  #in C
		# File Locations
		self.DBC_LOCATION = "../Final/DBC/DBC_Used_By_Application.dbc"
		self.TestCode_location = "../Final/Binary/bms_bq769x2_s32k11x_50.hex"
		self.Bootloader_location = "../Final/Binary/bootloader" + "_" + self.CAN_BAUDRATE + ".hex" 
		self.CodeFlash_location = dataConfig["CodeFlash_location"]

		"""Charger File Location """
		self.Charger_Bootloader_Loc = "../Final/Binary/N_B_STM32.hex"

		# PID and VID values
		self.PID_SCANNER = dataConfig["PID_SCANNER"]
		self.VID_SCANNER = dataConfig["VID_SCANNER"]
		self.tenmaLoad_PID      = 0x5011
		self.tenmaLoad_VID      = 0x416

		if self.PDI_MODE == 4:
			self.FCTBoard_PID = 0x7523
			self.FCTBoard_VID = 0x1A86
		
		else:
			self.FCTBoard_PID = 0x22CE
			self.FCTBoard_VID = 0x04D2
		
			
		self.CAN_BUS_VID = 0x042D
		self.CAN_BUS_PID = 0xA455

	
		self.PC_ID = 0x69
		self.CAN_ID = 0x10
		self.DB_PC_ID = 0x65
		
		"""Charger IDs"""
		# self.PC_ID=0x91
		self.CHARGER_DIAG_MESSAGE_CAN_ID=	0X1FF
		
		self.ALL_OK = 0
		self.REQUEST_DENIED = 2
		self.TEST_FAIL = 3
		self.TEST_PASS = 1

		# Commands To FCT BOARD
		self.Load_CHG	= 1
		self.CHG_DSG_PIN	= 2
		self.Load_DSG	= 3
		self.PING_FCT = 4
		self.EMULATOR_ON	= 5
		self.EMULATOR_OFF	= 6
		self.PACK_FUSE_SENSE_MODE	= 7
		self.DEFAULT_RELAT_MODE = 8
		self.AFE_POGO_PIN_STATE	= 9
		self.READ_BUCK	= 10
			# = 11
		self.H_WAKE	= 12
		self.L_WAKE	= 13
		self.DAUGHTER_BOARD_5V	= 14
		self.READ_1650_PIN = 15
		self.CURRENT_CALIBRATION_MODE = 16
		self.READ_REF_NTC = 17
		self.READ_UART_PIN = 18
			# = 19
			# = 20
			# = 21
			# = 22
			# = 23
			# = 24
			# = 25
		# Commands to BMS test Code
		self.PING_COMMAND	= 26
		self.NORFLASH_EEPROM_VERIFY	= 27
		self.FLASH_ECUID_1_LSB	= 28
		self.FLASH_ECUID_2_MSB	= 29
		self.Init_AFE	= 30
		self.DSG_FET_ON	= 31
		self.DSG_FET_OFF_DFET	= 32
		self.CHG_FET_ON	= 33
		self.CHG_FET_OFF_CFET	= 34
		self.READ_CURRENT	= 35
		self.AFE_SHUTDOWN	= 36
		self.BUCK_PIN_ON_MCU	= 37
		self.BUCK_PIN_OFF_MCU	= 38
		self.H_WAKE_FROM_MCU	= 39
		self.CALIBRATION_OFFSET = 40
		self.CALIBRATION_VALUE = 41
		self.CALIBRATION_GAIN = 42
		self.PIN_HIGH_1650 = 44
		self.PIN_LOW_1650 = 43
		self.UART_Loopback = 45
		self.PACK_FUSE_PIN_VALUE  = 46
		self.AFE_DIE_TEMP_1	= 47
		self.AFE_DIE_TEMP_2	= 48
		self.MCU_DIE_TEMP	= 49
		self.WRITE_PIN_CAN_STDBY_LOW	= 50
		self.WRITE_PIN_CAN_STDBY_HIGH	= 51
		self.PACK_LD_PIN_READ	= 52
		self.BMS_RESET = 53

		self.BUZZER_7SEGMENT = 61
		self.BOOT_MODE = 62
		self.RESET_MODE = 63

		""" Charger Test Command """
		self.CH_TESTCODE_FLASH_ADDR     = 0
		self.CH_BOOTLOADER_FLASH_ADDR   = 0x08000000
		self.CH_CUTOMER_FLASH_ADDR		= 0x08005800

		self.AC_IN_SUPPLY_READ    	=   71
		self.POS_12V_READ 	       	=   72
		self.POS_5V_READ		   	=   73
		self.TRML_CRT_READ			= 	74
		self.TRML_VTG_READ			=	75
		self.LLC_TRANS_TEMP_READ	=	76
		self.LLC_MOS_TEMP_READ		=	77
		self.LLC_DIODE_TEMP_READ	=	78
		self.PFC_MOS_TEMP_READ		=	79
		self.FCT_TEMP_READ			=	80
		self.TURN_ON_CHARGER		=	81
		self.TURN_OFF_CHARGER		=	82

		self.AC_IN_SUPPLY_CAN		=	83
		self.TRML_CRT_CAN			=	84		
		self.TRML_VTG_CAN			=	85		
		self.MAX_TEMP_CAN			=	86					

		self.Version = "v"+latestVersion.version #dataConfig["Version"]
		
		self.DEFAULT_REQUEST_TIMEOUT = 1 #In seconds

		# if(self.PDI_MODE==5):
		# 	ret=self.Charger_Final_Code_Loc.find("SMART")
		# 	self.CHARGER_SW_VERSION	=self.Charger_Final_Code_Loc[ret+6:ret+9]

		self.CAN_READ_COUNT = 5
		if self.PDI_MODE == 1:
			self.TEST_COUNT = len(self.TEST_ARRAY_PDI)
			self.TEST_ARRAY = self.TEST_ARRAY_PDI[0:self.TEST_COUNT]
			self.REF_NTC = [41]*self.THERM_COUNT
			self.REF_VOLTAGES = [self.EMULATOR_Voltage]*self.BMS_CELLCOUNT#[3430, 3380, 3393, 3032, 3228, 3151, 3158, 3161, 3322, 3293, 3105, 3151, 3322, 3238, 3250, 3336, 3365, 3440, 3250, 3268]#[4096]*24
			self.PRECHARGE_VALUE = "PreChargeFailed"
			self.MOSFET_VALUE_DISCHARGE = "Disabled"
		elif self.PDI_MODE == 2:
			self.TEST_COUNT = len(self.TEST_ARRAY_CODEFLASH)
			self.TEST_ARRAY = self.TEST_ARRAY_CODEFLASH[0:self.TEST_COUNT]
			self.REF_NTC = [41] * 6
			self.REF_VOLTAGES = [4096]*24
			self.PRECHARGE_VALUE = "PreChargeSuccessful"
			self.MOSFET_VALUE_DISCHARGE = "Enabled"
		elif self.PDI_MODE == 4:
			self.TEST_COUNT = len(self.TEST_ARRAY_DAUGHTERBOARD)
			self.TEST_ARRAY = self.TEST_ARRAY_DAUGHTERBOARD[0:self.TEST_COUNT]
			self.PRECHARGE_VALUE = "PreChargeSuccessful"
			self.MOSFET_VALUE_DISCHARGE = "Enabled"
			self.PC_ID = self.DB_PC_ID
		elif self.PDI_MODE == 5:
			self.TEST_COUNT = len(self.TEST_ARRAY_FCT_Charger)
			self.TEST_ARRAY = self.TEST_ARRAY_FCT_Charger[0:self.TEST_COUNT]
			self.PRECHARGE_VALUE = "PreChargeSuccessful"
			self.MOSFET_VALUE_DISCHARGE = "Enabled"
		else:
			if self.PDI_MODE == 3:
				self.TEST_COUNT = len(self.TEST_ARRAY_NO_FCT)
				self.TEST_ARRAYEST_ARRAY = self.TEST_ARRAY_NO_FCT[0:self.TEST_COUNT]
			else:
				self.TEST_COUNT = len(self.TEST_ARRAY_FCT)
				self.TEST_ARRAY = self.TEST_ARRAY_FCT[0:self.TEST_COUNT]
			self.REF_NTC = [41]*self.THERM_COUNT
			self.REF_VOLTAGES = [self.EMULATOR_Voltage]*self.BMS_CELLCOUNT
			self.PRECHARGE_VALUE = "PreChargeSuccessful"
			self.MOSFET_VALUE_DISCHARGE = "Enabled"

		self.STDCAN_VOLTAGE_KEYS = [
			"StacknCellVoltage_1",
			"StacknCellVoltage_2",
			"StacknCellVoltage_3",
			"StacknCellVoltage_4",
			"StacknCellVoltage_5",
			"StacknCellVoltage_6",
			"StacknCellVoltage_7",
			'StacknCellVoltage_8',
			"StacknCellVoltage_9",
			"StacknCellVoltage_10",
			"StacknCellVoltage_11",
			"StacknCellVoltage_12",
			"StacknCellVoltage_13",
			"StacknCellVoltage_14",
			"StacknCellVoltage_15",
			'StacknCellVoltage_16',
			'StacknCellVoltage_17',
			"StacknCellVoltage_18",
			"StacknCellVoltage_19",
			"StacknCellVoltage_20",
			"StacknCellVoltage_21",
			"StacknCellVoltage_22",
			"StacknCellVoltage_23",
			"StacknCellVoltage_24",
		]
		self.STDCAN_NTC_KEYS = [
			"Stack_Temp1",
			"Stack_Temp2",
			"Stack_Temp3",
			"Stack_Temp4",
			"Stack_Temp5",
			"Stack_Temp6",
			"Stack_Temp7",
		]

		self.OTHER_KEYS_1 = [
			["Precharge_Temp", 25, self.NTC_ACCURACY],
			["MOSFET_Temp", 25, self.NTC_ACCURACY],
			["ECUSerialNumber_Part1","", 0],
			["PackCurrent", self.LOAD_CURRENT_DEFAULT, self.CURRENT_ACCURACY],
			["MOSFETStatusPrecharge", self.PRECHARGE_VALUE, 0],
			["MOSFETStatusCharge","Enabled", 0],
			["MOSFETStatusDischarge",self.MOSFET_VALUE_DISCHARGE, 0],
			['OverVoltageFault', 0, 0],
			['UnderVoltageFault', 0, 0],
			['OverCurrentDischargeFault', 0, 0],
			['OverCurrentChargeFault', 0, 0],
			['ShortCircuitFault', 0, 0],
			['MOSFETOverTemperatureFault',0 , 0],
			['MOSFETUnderTemperatureFault', 0, 0]
		]
		if self.CAN_PROTOCOL == "J1939":
			self.CAN_Protocol_specific_keys=[
				['BoardUnderTemperatureFault', 0, 0],
				['OverTemperatureChargeFault', 0, 0],
				['UnderTemperatureChargeFault', 0, 0],
				['BatteryThermalRunaway', 0, 0],
				['OverTemperatureDischargeFault', 0, 0],
				['UnderTemperatureDischargeFault', 0, 0],
				['AFE_Comm_Error', 0, 0]
			]
		elif self.CAN_PROTOCOL == "STDCAN":
			self.CAN_Protocol_specific_keys=[
				['OverTemperatureFault', 0, 0],
				['UnderTemperatureFault', 0, 0],
			]
	
	def createRefArray(self):
		keyArray = [""] * 50
		i=0
		while i < self.BMS_CELLCOUNT:
			tempArray = [""] * 3
			tempArray[0] = self.STDCAN_VOLTAGE_KEYS[i]
			tempArray[1] = self.REF_VOLTAGES[i]
			if i in [0, (self.BMS_CELLCOUNT-1)]:
				tempArray[2] = self.VOLTAGE_ACCURACY + 100
			else:
				tempArray[2] = self.VOLTAGE_ACCURACY
			keyArray[i]= tempArray
			i += 1
		j=0
		while j<(self.THERM_COUNT):
			tempArray = [""] * 3
			tempArray[0] = self.STDCAN_NTC_KEYS[j]
			tempArray[1] = self.REF_NTC[j]
			tempArray[2] = self.NTC_ACCURACY
			keyArray[i] = tempArray
			i += 1
			j += 1
		
		j=0
		while j<(len(self.OTHER_KEYS_1)):
			keyArray[i] = self.OTHER_KEYS_1[j]
			i += 1
			j += 1
		
		j=0
		while j<(len(self.CAN_Protocol_specific_keys)):
			keyArray[i] = self.CAN_Protocol_specific_keys[j]
			i += 1
			j += 1
		
		return keyArray[0:i]

	def errorLogFileOpen(self,ID):
		global f
		f.close()
		tS = "FCT"
		if self.PDI_MODE == 1:
			tS = "PDI"
		elif self.PDI_MODE == 2:
			tS = "CF"
		elif self.PDI_MODE == 4:
			tS = "DB"
		elif self.PDI_MODE == 5:
			tS = "CFCT"
		f = open( "../../production_logs/"+ tS + self.Version +"Log_" + datetime.now().strftime("%d-%m-%Y")+ "_" + ID[2:] + ".txt","a")

	def errorLogFileClose(self):
		global f
		f.close()

	def errorLogger(self,ecuid="*",test="",msg="",count=1, testTime=0):
		global f
		if(count==0):
			f.write("-------------------------------------------------------------------")
			toWrite = "\n\nECUID"+": "+ecuid + "\n"
		else:
			toWrite = datetime.now().strftime("%d-%m-%Y %H:%M:%S")+", "+str(test)+", "+str(msg) + ", Time: " + str(float(testTime))+"\n"
		print(toWrite)
		f.write(toWrite)
		
	def excelLogFileOpen(self,ID, qrcode, logicqr, ecuid):
			global f2
			tS = "FCT"
			if self.PDI_MODE == 1:
				tS = "PDI"
			elif self.PDI_MODE == 2:
				tS = "CF"
			elif self.PDI_MODE == 4:
				tS = "DB"
			elif self.PDI_MODE == 5:
				tS = "CFCT"
			fileName = "../../production_logs/"+ tS + self.Version +"Log_" + datetime.now().strftime("%d-%m-%Y")+ "_" + ID[2:] + ".csv"
			if os.path.isfile(fileName) == False:
				f2 = open(fileName ,"a")
				if(self.PDI_MODE == 5):
					f2.write("Timestamp, POWER_QR, LOGIC_QR, ECUID, COMMIT_ID, SW_VERSION, ")
				else:	
					f2.write("Timestamp, QRCode, ECUID,")
				for i in range(len(self.TEST_ARRAY)):
					f2.write(self.TEST_ARRAY[i])
					f2.write(", ")
				f2.write("\n")
			else:
				f2 = open(fileName ,"a")
			
			# f2.write("\n")
			f2.write(datetime.now().strftime("%d-%m-%Y %H:%M:%S"))
			f2.write(", ")
			f2.write(qrcode)
			if(self.PDI_MODE==5):
				f2.write(", ")
				f2.write(logicqr)
			f2.write(", 0x")
			f2.write(ecuid)
			f2.write(", ")
			if(self.PDI_MODE==5):
				f2.write(self.Charger_Commit_ID)
				f2.write(", ")
				f2.write(str(self.CHARGER_SW_VERSION))
				f2.write(", ")
		
	def excelLogFileClose(self):
		global f2
		f2.write("\n")
		f2.close()

	def excelLogger(self,count, status):
		global f2
		toWrite = status + ", "
		print(toWrite)
		f2.write(toWrite)

