import serial.tools.list_ports
from datetime import datetime
import time
import FCTMacro as FM
FCTMacro = FM.FCTMacro()
class TenmaLoad():
    def __init__(self, pid=20497, vid=1046, result=serial.tools.list_ports.comports()):
        self.reslt          = result
        self.PID            = pid
        self.VID            = vid
        self.loadInstance   = None

    def loadConnect(self, retries = 5):
        if(self.reslt):
            for ports in self.reslt:
                if(ports.vid == self.VID and ports.pid == self.PID):
                    self.loadInstance = serial.Serial(ports.device, baudrate=115200)                                                                 #Initialize COM Port object
                    tries = 0
                    while(tries <= retries):
                        tries += 1
                        self.loadInstance.reset_input_buffer()
                        self.loadInstance.reset_output_buffer()
                        buffer = "*IDN?\n"
                        self.loadInstance.write(buffer.encode())
                        time.sleep(0.100)
                        if(self.loadInstance.in_waiting > 0):
                            retVal = self.loadInstance.readline()[:-1].decode('utf-8')
                            # print(retVal)
                            if(retVal.find("TENMA") >= 0):
                                FCTMacro.errorLogger(test="CC Load Present ", msg=str(1))
                                # print("CC Load Present")
                                return 0
                    return 1
            else:
                return 2
        else:
            return 3

    def getVoltage(self):
        buffer = ':MEASure:VOLTage?\n'
        # print(buffer[:-1])
        retVal = float(self.read_Data(buffer)[:-2])
        if(retVal):
            # print(retVal[:-1])
            return retVal
        return 0
    
    def read_Stat(self):
        buffer = ':INP?\n'
        # print(buffer[:-1])
        retVal = self.read_Data(buffer).decode('utf-8')
        if(retVal):
            # print(retVal[:-1])
            return retVal[:-1]

    def read_Curr(self):
        buffer = ':CURR?\n'
        # print(buffer[:-1])
        retVal = float(self.read_Data(buffer)[:-2])
        if(retVal):
            return retVal
    
    def read_FlowCurr(self):
        buffer = ':MEASure:CURRent?\n'
        # print(buffer[:-1])
        retVal = float(self.read_Data(buffer)[:-2])
        return retVal

    def read_Func(self):
        buffer = ':FUNC?\n'
        # print(buffer[:-1])
        retVal = self.read_Data(buffer).decode('utf-8')
        if(retVal):
            # print(retVal[:-1])
            return retVal[:-1]

    def read_Data(self, buff, retries = 10):
        tries = 0
        while(tries <= retries):
            self.loadInstance.reset_input_buffer()
            self.loadInstance.reset_output_buffer()
            tries += 1
            buffer = buff
            self.loadInstance.write(buffer.encode())
            time.sleep(0.100)
            startTime = time.perf_counter()
            data = None
            # print("Command:{0},Tries:{1}".format(buff[:-1],str(tries)))
            while((time.perf_counter() - startTime) < 0.2):
                # print(time.perf_counter())
                if(self.loadInstance.in_waiting > 0):
                    data = (self.loadInstance.readline())
                    return data
            
        return None

    # def setCurr(self, value = 2, retries = 2):
    #     try:
    #         tries = 0
    #         while(tries < retries):
    #             tries += 1
    #             curr = self.read_Curr()
    #             time.sleep(0.100)
    #             mode = self.read_Func()
    #             if(curr != float(value) or mode != "CC"):
    #                 buffer = ':CURR {0}A\n'.format(str(value))
    #                 # print(buffer)
    #                 self.loadInstance.write(buffer.encode())
    #                 time.sleep(0.100)
    #                 curr = self.read_Curr()
    #                 time.sleep(0.100)
    #                 mode = self.read_Func()
    #                 if(curr == float(value) and mode == "CC"):
    #                     time.sleep(0.100)
    #                     inp = self.read_Stat()
    #                     if(inp != "ON"):
    #                         buffer = ':INP 1\n'
    #                         time.sleep(0.100)
    #                         self.loadInstance.write(buffer.encode())
    #                         time.sleep(0.100)
    #                         inp = self.read_Stat()
    #                         if(inp == "ON"):
    #                             return 1
    #             else:
    #                 time.sleep(0.100)
    #                 inp = self.read_Stat()
    #                 if(inp != "ON"):
    #                     buffer = ':INP 1\n'
    #                     time.sleep(0.100)
    #                     self.loadInstance.write(buffer.encode())
    #                     time.sleep(0.100)
    #                     inp = self.read_Stat()
    #                     if(inp == "ON"):
    #                         return 1
    #                 else:
    #                     return 1
    #         return 0
    #     except Exception as e:
    #         FCTMacro.errorLogger(test="setCurr exception", msg=str(e))
    #         return 0

    def setLoadCurrent(self, value = 2, delay = 0, mode = "CC"):
        try:
            if mode == "CR":
                buffer = ':RES {0}OHM\n'.format(str(value))
                self.loadInstance.write(buffer.encode())
            else:   
                curr = self.read_Curr()
                # time.sleep(0.100)
                mode = self.read_Func()
                # print(mode)
                if(curr != float(value) or mode != "CC"):
                    buffer = ':CURR {0}A\n'.format(str(value))
                    self.loadInstance.write(buffer.encode())
                    # time.sleep(0.100)
                    curr = self.read_Curr()
                    # time.sleep(0.100)
                    mode = self.read_Func()
                    if curr == None:
                        curr = 0.0
                    if curr != float(value):
                        return 1
                    if mode != "CC":
                        return 2
                # time.sleep(0.100)
            inp = self.read_Stat()
            if(inp != "ON"):
                buffer = ':INP 1\n'
                # time.sleep(0.100)
                self.loadInstance.write(buffer.encode())
                # time.sleep(0.100)
                inp = self.read_Stat()
                if(inp != "ON"):
                    return 3
            time.sleep(delay)
            return 0
        except Exception as e:
            FCTMacro.errorLogger(test="sendCommandBMS exception", msg=str(e))
            return 1

    def setLoadResistance(self, value = 20, delay = 0):
        curr = self.read_Curr()
        # time.sleep(0.100)
        mode = self.read_Func()
        print(mode)
        if(curr != float(value) or mode != "CR"):
            buffer = ':RES {0}OHM\n'.format(str(value))
            resp = self.loadInstance.write(buffer.encode())
            # time.sleep(0.100)
            # time.sleep(0.100)
            mode = self.read_Func()
            if mode != "CR":
                return 2
            # curr = self.read_Curr()
            # if curr == None:
            #     curr = 0.0
            # if curr != float(value):
            #     return 1
        # time.sleep(0.100)
        inp = self.read_Stat()
        if(inp != "ON"):
            buffer = ':INP 1\n'
            # time.sleep(0.100)
            self.loadInstance.write(buffer.encode())
            # time.sleep(0.100)
            inp = self.read_Stat()
            if(inp != "ON"):
                return 3
        time.sleep(delay)
        return 0