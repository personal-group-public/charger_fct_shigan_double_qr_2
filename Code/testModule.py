from tracemalloc import start
import serial.tools.list_ports
import time
from datetime import datetime
import FCTMacro as FM
FCTMacro = FM.FCTMacro()
class test_Module():
    def __init__(self,result=serial.tools.list_ports.comports()):
        self.reslt = result
        self.PID = FCTMacro.FCTBoard_PID
        self.VID = FCTMacro.FCTBoard_VID
        self.ECUID = ""
        self.count = 0

    def sendCommandFCTBoard(self, ser, command, data=[0,0,0,0], timeout=FCTMacro.DEFAULT_REQUEST_TIMEOUT, tries = 3):
        try:
            if ser == 0:
                return 0
            dataToSend = [0] * 8
            dataToSend[0] = ord('$')
            dataToSend[1] = FCTMacro.PC_ID
            dataToSend[2] = command
            for i in range(4):
                dataToSend[3+i]=data[i]
            dataToSend[7] = ord('#')
            retries = 0
            # print(dataToSend)
            while(retries < tries):
                ser.reset_input_buffer()
                ser.reset_output_buffer()
                ser.write(dataToSend)
                startTime = time.time()
                dataRecv = 0
                while 1:
                    currTime = time.time()
                    if ser.in_waiting > 0:
                        dataRecv = 1
                        retries = tries + 1
                        break
                    if (currTime - startTime) >= timeout:
                        retries += 1
                        break
            if dataRecv == 1:
                temp = ser.read(8)
                intTemp = [0]*8
                for i in range(8):
                    intTemp[i] = int(temp[i])
                FCTMacro.errorLogger(test="sendCommandFCTBoard: "+str(command), msg=intTemp)
                if temp[1] != command:
                    return 0
                else:
                    return temp
            else:
                return 0
        except Exception as e:
            FCTMacro.errorLogger(test="sendCommandFCTBoard exception", msg=str(e))
            return 0

    def moduleConnect(self):
        if(self.reslt):
            for ports in self.reslt:
                if(ports.vid == self.VID and ports.pid == self.PID):
                    FCTMacro.errorLogger(test="FCTBoard Found ", msg=str(ports))
                    # print("FCTBoard Found ", end="")
                    # print(ports)
                    return ports.device
            else:
                return 1
        else:
            return 2
